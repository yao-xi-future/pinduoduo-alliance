FROM anapsix/alpine-java:8_server-jre_unlimited
VOLUME /tmp
MAINTAINER dou631@163.com
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
RUN mkdir -p /opt/projects/pss-mall
WORKDIR /opt/projects/pss-mall
#ADD ./pss-mall-admin/target/pss-mall-admin-0.0.1-SNAPSHOT.jar ./
ADD admin-ups.jar ./
EXPOSE 5000
#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","pss-mall-admin-0.0.1-SNAPSHOT.jar","--spring.profiles.active=docker,quartz"]
#CMD java -jar -XX:MetaspaceSize=512m -XX:MaxMetaspaceSize=512m -Xms512m -Xmx512m -Xmn128m -Xss256k -XX:SurvivorRatio=8 -XX:+UseConcMarkSweepGC -Dspring.profiles.active=test admin-ups.jar
CMD java -jar -Dspring.profiles.active=test blog_sh.jar