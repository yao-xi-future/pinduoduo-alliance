package com.cloud.pdd.controller;

import com.cloud.pdd.model.domain.UserInfo;
import com.cloud.pdd.model.request.PddProductInfoRequest;
import com.cloud.pdd.service.PddUsersService;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkGoodsDetailResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("product/convert")
public class ProductConvertController extends BaseController {
    @GetMapping("")
    public ModelAndView convert(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView view = baseView("拼多多商品转链", "product/convert");
        return view;
    }
}
