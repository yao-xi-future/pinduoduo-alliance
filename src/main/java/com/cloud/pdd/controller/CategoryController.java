package com.cloud.pdd.controller;

import cn.hutool.core.convert.Convert;
import com.cloud.pdd.model.enums.ProductCategoryEnum;
import com.cloud.pdd.model.request.PddProductSearchRequest;
import com.cloud.pdd.service.PddCategoryService;
import com.cloud.pdd.service.PddProductService;
import com.cloud.pdd.util.PddUtil;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkGoodsSearchResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Controller
@AllArgsConstructor
@RequestMapping("product")
public class CategoryController extends BaseController {
    private final PddCategoryService pddCategoryService;
    private final PddProductService pddProductService;
    private final PddUtil pddUtil;

    /**
     * 拼多多首页
     */
    @GetMapping("category")
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response,
                              Integer optId) {
        ModelAndView view = baseView("商品分类-拼多多", "product/category");
        if (optId == null) {
            //新增默认分类ID
            optId = 8569;
        }
        Integer finalCurrentId = optId;
        Map<String, String> map = ProductCategoryEnum.getAllEnum().stream().filter(
                t -> Convert.toInt(t.get("code")).equals(Convert.toInt(finalCurrentId))).findFirst().orElse(null);
        String categoryName = "";
        if (map != null) {
            categoryName = Convert.toStr(map.get("desc"));
        }
        view.addObject("categoryName", categoryName);
        view.addObject("allCategory", pddCategoryService.getCategoryListHtml(optId));
        PddProductSearchRequest pddProductSearchRequest = new PddProductSearchRequest();
        pddProductSearchRequest.setOptId(Convert.toLong(optId));
        pddProductSearchRequest.setPageSize(36);
        PddDdkGoodsSearchResponse.GoodsSearchResponse productSearch = pddProductService.getProductSearch(pddProductSearchRequest);
        List<PddDdkGoodsSearchResponse.GoodsSearchResponseGoodsListItem> goodsList = productSearch.getGoodsList();
        view.addObject("data", getProductList(goodsList));
        view.addObject("pddUtil", pddUtil);
        return view;
    }

    private String getProductList(List<PddDdkGoodsSearchResponse.GoodsSearchResponseGoodsListItem> goodsList) {
        StringBuilder sb = new StringBuilder();
        int maxSize = goodsList.size();
        for (int i = 0; i < goodsList.size(); i++) {
            PddDdkGoodsSearchResponse.GoodsSearchResponseGoodsListItem goodsSearchResponseGoodsListItem = goodsList.get(i);
            if (i == 0 || i % 3 == 0) {
                sb.append("<ul>");
            }
            String detailUrl = pddUtil.getDetailUrl(goodsSearchResponseGoodsListItem.getGoodsSign(), goodsSearchResponseGoodsListItem.getSearchId());
            sb.append("<li>");
            sb.append("<a href=\"" + detailUrl + "\">");
            sb.append("<img class=\"img_lazy\" src=\"" + goodsSearchResponseGoodsListItem.getGoodsThumbnailUrl() + "\"><br>");
            sb.append("<span>" + goodsSearchResponseGoodsListItem.getGoodsName() + "</span>");
            sb.append("</a>");
            sb.append("</li>");
            if (i != 0 && (i + 1) % 3 == 0 || i == maxSize) {
                sb.append("</ul>");
            }
        }
        sb.append("<span id='span_end'></span>");
        return sb.toString();

    }

}
