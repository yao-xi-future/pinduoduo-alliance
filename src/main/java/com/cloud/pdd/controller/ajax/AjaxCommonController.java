package com.cloud.pdd.controller.ajax;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.img.ImgUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cloud.common.oss.AliOssRequest;
import com.cloud.common.oss.AliOssResponse;
import com.cloud.common.oss.AliUploadHelper;
import com.cloud.common.util.R;
import com.cloud.pdd.config.AliOssConfig;
import com.cloud.pdd.model.domain.UserInfo;
import com.cloud.pdd.model.entity.PddUsers;
import com.cloud.pdd.service.PddUsersService;
import com.cloud.pdd.util.PddUtil;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

@AllArgsConstructor
@RestController
@RequestMapping("ajax/common")
public class AjaxCommonController {

    private final PddUsersService pddUsersService;
    private final PddUtil pddUtil;
    private final AliOssConfig aliOssConfig;

    /**
     * 上传blob方式
     */
    @PostMapping("upload_blob")
    public R upload_blob(HttpServletRequest request) throws IOException {
        R r = new R();
        String base64file = request.getParameter("base64file");
        String fileData = "";
        String[] splitFile = StrUtil.split(base64file, ",");
        if (splitFile.length > 1) {
            fileData = splitFile[1];
        }
        BufferedImage bufferedImage = ImgUtil.toImage(fileData);
        InputStream inputStream = bufferedImageToInputStream(bufferedImage);
        UserInfo userInfo = pddUsersService.getUserInfo(request);
//        String typeName = request.getParameter("filedir");
        String typeName = "pdd";
        String filedir = pddUtil.createOssFiledir(typeName, "jpg");
        AliOssRequest aliOssRequest = new AliOssRequest();
        BeanUtil.copyProperties(aliOssConfig, aliOssRequest);
        AliUploadHelper aliUploadHelper = new AliUploadHelper(aliOssRequest);
        AliOssResponse aliOssResponse =
                aliUploadHelper.uploadSimple(inputStream, filedir);
        r.setData(aliOssResponse.getAbsolutePath());
        r.setMsg("上传成功");
        setSinglesHeadPhoto(userInfo, aliOssResponse.getAbsolutePath());
        return r;
    }

    /**
     * 设置头象
     */
    private void setSinglesHeadPhoto(UserInfo userInfo, String photoPath) {
        PddUsers pddUsers = new PddUsers();
        pddUsers.setId(userInfo.getId());
        pddUsers.setPhoto(photoPath);
        pddUsersService.updateById(pddUsers);
    }

    /**
     * 将BufferedImage转换为InputStream
     *
     * @param image
     * @return
     */
    public InputStream bufferedImageToInputStream(BufferedImage image) {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            ImageIO.write(image, "jpg", os);
            InputStream input = new ByteArrayInputStream(os.toByteArray());
            return input;
        } catch (IOException e) {
        }
        return null;
    }
}
