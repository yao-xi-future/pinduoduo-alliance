package com.cloud.pdd.controller.ajax;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cloud.common.security.AES;
import com.cloud.common.util.CookieUtils;
import com.cloud.common.util.R;
import com.cloud.pdd.config.Config;
import com.cloud.pdd.model.domain.UserInfo;
import com.cloud.pdd.model.entity.PddUserBankcards;
import com.cloud.pdd.model.entity.PddUsers;
import com.cloud.pdd.model.entity.PddWithdrawalOrders;
import com.cloud.pdd.model.enums.CookieConst;
import com.cloud.pdd.model.request.PddCenterUserPasswordRequest;
import com.cloud.pdd.service.PddUserBankcardsService;
import com.cloud.pdd.service.PddUsersService;
import com.cloud.pdd.service.PddWithdrawalOrdersService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("ajax/center")
@AllArgsConstructor
public class AjaxCenterController {
    private final PddUsersService pddUsersService;
    private final PddUserBankcardsService pddUserBankcardsService;
    private final PddWithdrawalOrdersService pddWithdrawalOrdersService;
    private final Config config;

    //修改用户信息
    @ResponseBody
    @PostMapping("modifyUserInfo")
    public R modifyUserInfo(HttpServletRequest request, HttpServletResponse response,
                            PddUsers req) {
        R r = new R();
        if (!StrUtil.isEmpty(req.getBirthdayStr())) {
            req.setBirthday(DateUtil.parse(req.getBirthdayStr()));
        }
        UserInfo userInfo = pddUsersService.getUserInfo(request);
        req.setId(userInfo.getId());
        pddUsersService.updateById(req);
        return r;
    }

    @ResponseBody
    @PostMapping("userinfopwd")
    public R userinfopwd(HttpServletRequest request, HttpServletResponse response,
                         PddCenterUserPasswordRequest req) {
        R r = new R();
        if (!req.getNewPassowrd().equals(req.getConfirmNewPassword())) {
            r.setCode(2);
            r.setMsg("两次新密码不一至，请确认后再次更新");
            return r;
        }
        UserInfo userInfo = pddUsersService.getUserInfo(request);
        //验证老密码
        Integer userId = userInfo.getId();
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("id", userId);
        queryWrapper.eq("password", AES.aesEncrypt(req.getOldPassword()));
        PddUsers oldPddUsers = pddUsersService.getOne(queryWrapper);
        if (oldPddUsers == null) {
            //旧密码错误
            r.setCode(1);
            r.setMsg("原密码错误，请确认后再次更新");
            return r;
        }
        PddUsers pddUsers = new PddUsers();
        pddUsers.setId(userId);
        pddUsers.setPassword(AES.aesEncrypt(req.getNewPassowrd()));
        pddUsersService.updateById(pddUsers);
        return r;
    }

    //绑卡
    @ResponseBody
    @PostMapping("bind")
    public R bind(HttpServletRequest request, HttpServletResponse response,
                  PddUserBankcards req) {
        UserInfo userInfo = pddUsersService.getUserInfo(request);
        Integer userId = userInfo.getId();
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id", userId);
        PddUserBankcards one = pddUserBankcardsService.getOne(queryWrapper);
        req.setUserId(userId);
//这里固定死是支付宝了
        req.setWay("1");
        if (one == null) {
            //不存在就创建
            pddUserBankcardsService.save(req);
        } else {
            //存在就修改
            pddUserBankcardsService.update(req, queryWrapper);
        }
        return new R();
    }

    //提现
    @ResponseBody
    @PostMapping("applyWithdraw")
    public R applyWithdraw(HttpServletRequest request, HttpServletResponse response,
                           PddWithdrawalOrders req) {
        R r = new R();
        UserInfo userInfo = pddUsersService.getUserInfo(request);
        Integer userId = userInfo.getId();
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id", userId);
        PddUserBankcards pddUserBankcards = pddUserBankcardsService.getOne(queryWrapper);
        if (pddUserBankcards == null) {
            r.setCode(1);
            r.setMsg("请先进行绑卡，现申请提现");
            return r;
        }
        req.setUserId(userId);
//这里固定死是支付宝了
        req.setWay(pddUserBankcards.getWay());
        req.setAccountName(pddUserBankcards.getName());
        req.setAccountNumber(pddUserBankcards.getNumber());
        int result = pddWithdrawalOrdersService.applyWithdraw(req);
        if (result == -2) {
            r.setCode(-2);
            r.setMsg("提现最小金额为 " + config.getMinWithdraw() + " 元");
            return r;
        }
        if (result == -1) {
            r.setCode(-1);
            r.setMsg("余额不足，提现申请失败");
            return r;
        }
        return new R();
    }

}
