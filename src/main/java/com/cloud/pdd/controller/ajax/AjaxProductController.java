package com.cloud.pdd.controller.ajax;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cloud.common.util.R;
import com.cloud.pdd.model.domain.ProductUrlGenRequest;
import com.cloud.pdd.model.domain.UserInfo;
import com.cloud.pdd.model.entity.PddFavorite;
import com.cloud.pdd.model.request.PddProductPriceRequest;
import com.cloud.pdd.model.request.PddProductSearchRequest;
import com.cloud.pdd.model.request.RegRequest;
import com.cloud.pdd.service.PddFavoriteService;
import com.cloud.pdd.service.PddProductService;
import com.cloud.pdd.service.PddUsersService;
import com.pdd.pop.sdk.http.api.pop.request.PddDdkGoodsPromotionUrlGenerateRequest;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkGoodsPromotionUrlGenerateResponse;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkGoodsSearchResponse;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkGoodsZsUnitUrlGenResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("ajax/product")
@AllArgsConstructor
public class AjaxProductController {

    private final PddProductService pddProductService;
    private final PddFavoriteService pddFavoriteService;
    private final PddUsersService pddUsersService;

    @ResponseBody
    @PostMapping("convert")
    public R convert(HttpServletRequest request,
                     HttpServletResponse response,
                     ProductUrlGenRequest req) {
        R r = new R();
        PddDdkGoodsZsUnitUrlGenResponse.GoodsZsUnitGenerateResponse productUnitUrlGenerate = pddProductService.getProductUnitUrlGenerate(request, req);
        if (productUnitUrlGenerate == null) {
            r.setMsg("转换失败，未找到优惠券");
            r.setCode(1);
            return r;
        }
        r.setData(productUnitUrlGenerate.getMultiGroupMobileShortUrl());
        return r;
    }

    @ResponseBody
    @PostMapping("getShare")
    public R getShare(HttpServletRequest request, HttpServletResponse response, PddProductPriceRequest req) {
        R r = new R();
        PddDdkGoodsPromotionUrlGenerateRequest pddDdkGoodsPromotionUrlGenerateRequest = new PddDdkGoodsPromotionUrlGenerateRequest();
        List<String> listGoodsId = new ArrayList<>();
        listGoodsId.add(req.getGoodsSign());
        pddDdkGoodsPromotionUrlGenerateRequest.setGoodsSignList(listGoodsId);
        PddDdkGoodsPromotionUrlGenerateResponse.GoodsPromotionUrlGenerateResponse productPromotionUrlGenerate
                = pddProductService.getProductPromotionUrlGenerate(pddDdkGoodsPromotionUrlGenerateRequest);
        r.setData(productPromotionUrlGenerate.getGoodsPromotionUrlList());
        return r;
    }

    //获取价格单 type=1拼团，type=2直接购买
    @ResponseBody
    @PostMapping("getPrice")
    public R getPrice(HttpServletRequest request, HttpServletResponse response, PddProductPriceRequest req) {
        R r = new R();
        String type = req.getType();
        PddDdkGoodsPromotionUrlGenerateRequest pddDdkGoodsPromotionUrlGenerateRequest = new PddDdkGoodsPromotionUrlGenerateRequest();
        List<String> listGoodsSign = new ArrayList<>();
        listGoodsSign.add(req.getGoodsSign());
        pddDdkGoodsPromotionUrlGenerateRequest.setGoodsSignList(listGoodsSign);
        pddDdkGoodsPromotionUrlGenerateRequest.setSearchId(req.getSearchId());
        PddDdkGoodsPromotionUrlGenerateResponse.GoodsPromotionUrlGenerateResponse productPromotionUrlGenerate =
                pddProductService.getProductPromotionUrlGenerate(pddDdkGoodsPromotionUrlGenerateRequest);
        pddDdkGoodsPromotionUrlGenerateRequest.setMultiGroup(type.equals("1") ? true : false);
        List<PddDdkGoodsPromotionUrlGenerateResponse.GoodsPromotionUrlGenerateResponseGoodsPromotionUrlListItem>
                goodsPromotionUrlList = productPromotionUrlGenerate.getGoodsPromotionUrlList();
        PddDdkGoodsPromotionUrlGenerateResponse.GoodsPromotionUrlGenerateResponseGoodsPromotionUrlListItem goodsPromotionUrlGenerateResponseGoodsPromotionUrlListItem =
                goodsPromotionUrlList.stream().findFirst().orElse(null);
        if (goodsPromotionUrlGenerateResponseGoodsPromotionUrlListItem == null) {
            r.setCode(1);
            return r;
        }
        r.setData(goodsPromotionUrlGenerateResponseGoodsPromotionUrlListItem.getMobileUrl());
        return r;
    }

    @ResponseBody
    @PostMapping("getData")
    public R getData(HttpServletRequest request, HttpServletResponse response, PddProductSearchRequest req) {
        R r = new R();
        PddDdkGoodsSearchResponse.GoodsSearchResponse productSearch = pddProductService.getProductSearch(req);
        List<PddDdkGoodsSearchResponse.GoodsSearchResponseGoodsListItem> goodsList = productSearch.getGoodsList();
        String productListData = pddProductService.getProductListData(req, goodsList);
        r.setData(productListData);
        r.setMsg(productSearch.getListId());
        r.setCode(0);
        return r;
    }

    @ResponseBody
    @PostMapping("myFavorite")
    public R myFavorite(HttpServletRequest request,
                        HttpServletResponse response,
                        PddFavorite req) {
        R r = new R();
        UserInfo userInfo = pddUsersService.getUserInfo(request);
        if (userInfo == null) {
            r.setCode(1);
            r.setMsg("请登录后收藏");
            r.setData("/login");
            return r;
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id", userInfo.getId());
        queryWrapper.eq("goods_id", req.getGoodsId());
        int count = pddFavoriteService.count(queryWrapper);
        if (count > 0) {
            //删除
            pddFavoriteService.remove(queryWrapper);
        } else {
            req.setUserId(userInfo.getId());
            pddFavoriteService.save(req);
        }
        r.setMsg("收藏成功");
        r.setCode(0);
        return r;
    }

    @ResponseBody
    @PostMapping("delFavorite")
    public R delFavorite(HttpServletRequest request,
                         HttpServletResponse response,
                         PddFavorite req) {
        R r = new R();
        UserInfo userInfo = pddUsersService.getUserInfo(request);
        if (userInfo == null) {
            r.setCode(1);
            r.setMsg("请登录后收藏");
            r.setData("/login");
            return r;
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id", userInfo.getId());
        queryWrapper.eq("id", req.getId());
        pddFavoriteService.remove(queryWrapper);
        r.setMsg("删除成功");
        r.setCode(0);
        return r;
    }
}
