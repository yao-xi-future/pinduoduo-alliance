package com.cloud.pdd.controller;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.cloud.pdd.model.enums.PddMerchantTypeEnum;
import com.cloud.pdd.model.enums.ProducServiceTagsEnum;
import com.cloud.pdd.model.enums.ProductCategoryEnum;
import com.cloud.pdd.model.request.PddProductInfoRequest;
import com.cloud.pdd.model.request.PddProductRecommendListRequest;
import com.cloud.pdd.model.request.PddProductSearchRequest;
import com.cloud.pdd.service.PddProductService;
import com.cloud.pdd.service.PddUsersService;
import com.cloud.pdd.util.PddUtil;
import com.pdd.pop.sdk.http.api.pop.request.PddDdkGoodsPromotionUrlGenerateRequest;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkGoodsDetailResponse;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkGoodsPromotionUrlGenerateResponse;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkGoodsRecommendGetResponse;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkGoodsSearchResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@AllArgsConstructor
@RequestMapping("product/list")
public class ProductListController extends BaseController {
    private final PddUsersService pddUsersService;
    private final PddProductService pddProductService;
    private final PddUtil pddUtil;

    @GetMapping("")
    public ModelAndView list(HttpServletRequest request, HttpServletResponse response,
                             PddProductSearchRequest req) {
        ModelAndView view = baseView("商品列表", "product/list");
        PddDdkGoodsSearchResponse.GoodsSearchResponse productSearch = pddProductService.getProductSearch(req);
        view.addObject("listId", productSearch == null ? "" : productSearch.getListId());
        List<PddDdkGoodsSearchResponse.GoodsSearchResponseGoodsListItem> goodsList = productSearch == null ? null : productSearch.getGoodsList();
        view.addObject("req", req);
        Integer showType = req.getShowType();
        if (showType == null) {
            showType = 2;
        }
        if (showType == 1) {
            view.addObject("listDataClass", "zy_goods_list");
        } else {
            view.addObject("listDataClass", "zy_goods_grid");
        }
        view.addObject("getListData", pddProductService.getProductListData(req, goodsList));
        view.addObject("pddUtil", pddUtil);
        view.addObject("getSort", getSort(req, showType));
        return view;
    }

    private String getSort(PddProductSearchRequest req, Integer showType) {
        Long optId = Convert.toLong(req.getOptId());
        if (optId == null) {
            //如果是空的就赋值为精品
            optId = ProductCategoryEnum.JINGXUAN.getCode();
        }
        StringBuilder sb = new StringBuilder();
        Integer sortType = Convert.toInt(req.getSortType(), 0);
        sb.append("<li class=\"" + (sortType == 0 ? "cur" : "") + "\"><a href=\"/product/list?optId=" + optId + "&showType=" + showType + "\">综合</a></li>");
        sb.append("<li class=\"" + (sortType == 6 ? "cur" : "") + "\"><a href=\"/product/list?optId=" + optId + "&sortType=6&showType=" + showType + "\">销量</a></li>");
        sb.append("<li class=\"price " + (sortType == 3 || sortType == 4 ? "cur" : "") + "\"><a href=\"/product/list?optId=" + optId + "&sortType=" + (sortType == 3 ? 4 : 3) + "\">价格</a><span><i class=\"\"></i><i></i></span></li>");
        showType = showType == 1 ? 2 : 1;
        String img = "/images/web/icon_list.png";
        if (showType == 1) {
            //如果是单行显示
            img = "/images/web/icon_grid.png";
        }
        sb.append("<li class=\"type\"><a href=\"/product/list?optId=" + optId + "&sortType=" + sortType + "&showType=" + showType + "\"><img src=\"" + img + "\"></a><span><i></i></span></li>");
        return sb.toString();
    }

}
