package com.cloud.pdd.controller;

import com.cloud.common.util.R;
import com.cloud.pdd.model.domain.UserInfo;
import com.cloud.pdd.model.request.PddOrderListRequest;
import com.cloud.pdd.service.PddOrderService;
import com.cloud.pdd.service.PddUsersService;
import com.cloud.pdd.model.request.RegRequest;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkOrderListIncrementGetResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@AllArgsConstructor
@RequestMapping("order")
public class OrderController extends BaseController {

    private final PddUsersService pddUsersService;
    private final PddOrderService pddOrderService;

    @GetMapping("list")
    public ModelAndView list(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView view = baseView("商品分销平台-拼多多", "home/index");
        UserInfo userInfo = pddUsersService.getUserInfo(request);
        view.addObject("userInfo", userInfo);
        return view;
    }

    @GetMapping("orderSync")
    @ResponseBody
    public R orderSync(HttpServletRequest request, HttpServletResponse response) {
        //同步x天的订单数据
        pddOrderService.syncOrder(8);
        pddOrderService.pay();
        return new R();
    }
}
