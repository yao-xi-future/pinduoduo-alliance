package com.cloud.pdd.controller;

import com.cloud.pdd.model.domain.UserInfo;
import com.cloud.pdd.model.enums.*;
import com.cloud.pdd.model.request.PddProductRecommendListRequest;
import com.cloud.pdd.model.request.PddProductSearchRequest;
import com.cloud.pdd.service.PddCategoryService;
import com.cloud.pdd.service.PddProductService;
import com.cloud.pdd.service.PddUsersService;
import com.cloud.pdd.util.PddUtil;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkGoodsRecommendGetResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
/*
 * ICO 源地址：https://www.easyicon.net/1139182-book_icon.html
 *
 * */

@Controller
@AllArgsConstructor
@RequestMapping("")
public class HomeController extends BaseController {

    private final PddUsersService pddUsersService;
    private final PddProductService pddProductService;
    private final PddCategoryService pddCategoryService;
    private final PddUtil pddUtil;

    /**
     * 拼多多首页
     */
    @GetMapping("")
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView view = baseView("商品分销平台-拼多多", "home/index");
        UserInfo userInfo = pddUsersService.getUserInfo(request);
        view.addObject("userInfo", userInfo);
        view.addObject("allCategory", pddCategoryService.getHomeCategoryHtml());
        view.addObject("getJinRiXiaoLiangBang", getJinRiXiaoLiangBang());
        view.addObject("getShiShiReXiaoBang", getShiShiReXiaoBang());
        view.addObject("getCaiNiXiHuan", getCaiNiXiHuan());
        view.addObject("getZhuanQuJingXuan", getZhuanQuJingXuan());

        view.addObject("getZhuanQuBaiHuo", getZhuanQuBaiHuo());
        view.addObject("getZhuanQuShiPin", getZhuanQuShiPin());
        view.addObject("getZhuanQuMuYing", getZhuanQuMuYing());
        view.addObject("getZhuanQuNvZhuang", getZhuanQuNvZhuang());
        view.addObject("getZhuanQuMeiZhuang", getZhuanQuMeiZhuang());
        view.addObject("getZhuanQuDianQi", getZhuanQuDianQi());
        view.addObject("getZhuanQuXieBao", getZhuanQuXieBao());
        view.addObject("getZhuanQuXieBao", getZhuanQuNeiYi());
        view.addObject("getZhuanQuShuiGuo", getZhuanQuShuiGuo());
        view.addObject("getJingXuanBaoPin", getJingXuanBaoPin());
        view.addObject("pddUtil", pddUtil);
        return view;
    }

    //获取首页今日销量榜
    private List<PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponseListItem> getJinRiXiaoLiangBang() {
        PddProductRecommendListRequest pddProductRecommendListRequest = new PddProductRecommendListRequest();
        pddProductRecommendListRequest.setChannelType(ProductChannelTypeEnum.JINRIXIAOLIANGBANG.getCode());
        pddProductRecommendListRequest.setLimit(3);
        PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponse productRecommend = pddProductService.getProductRecommend(pddProductRecommendListRequest);
        return productRecommend.getList();
    }

    //获取首页今日销量榜
    private List<PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponseListItem> getShiShiReXiaoBang() {
        PddProductRecommendListRequest pddProductRecommendListRequest = new PddProductRecommendListRequest();
        pddProductRecommendListRequest.setChannelType(ProductChannelTypeEnum.SHISHIREXIAOBANG.getCode());
        pddProductRecommendListRequest.setLimit(6);
        PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponse productRecommend = pddProductService.getProductRecommend(pddProductRecommendListRequest);
        return productRecommend.getList();
    }

    //获取首页猜你喜欢
    private List<PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponseListItem> getCaiNiXiHuan() {
        PddProductRecommendListRequest pddProductRecommendListRequest = new PddProductRecommendListRequest();
        pddProductRecommendListRequest.setChannelType(ProductChannelTypeEnum.CAINIXIHUAN.getCode());
        pddProductRecommendListRequest.setLimit(10);
        //百货
//        pddProductRecommendListRequest.setCatId(20100L);
        PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponse productRecommend = pddProductService.getProductRecommend(pddProductRecommendListRequest);
        return productRecommend == null ? null : productRecommend.getList();
//        return productRecommend.getList();
    }

    //获取首页精选
    private List<PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponseListItem> getZhuanQuJingXuan() {
        PddProductRecommendListRequest pddProductRecommendListRequest = new PddProductRecommendListRequest();
        pddProductRecommendListRequest.setChannelType(ProductChannelTypeEnum.SHISHIREXIAOBANG.getCode());
//        pddProductRecommendListRequest.setCatId(ProductGuessCategoryEnum.JINGXUAN.getCode());
        pddProductRecommendListRequest.setLimit(6);
        PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponse productRecommend = pddProductService.getProductRecommend(pddProductRecommendListRequest);
        return productRecommend.getList();
    }

    //获取首页百货
    private List<PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponseListItem> getZhuanQuBaiHuo() {
        PddProductRecommendListRequest pddProductRecommendListRequest = new PddProductRecommendListRequest();
        pddProductRecommendListRequest.setChannelType(ProductChannelTypeEnum.CAINIXIHUAN.getCode());
        pddProductRecommendListRequest.setCatId(ProductGuessCategoryEnum.BAIHUO.getCode());
        pddProductRecommendListRequest.setLimit(6);
        PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponse productRecommend = pddProductService.getProductRecommend(pddProductRecommendListRequest);
        return productRecommend == null ? null : productRecommend.getList();
    }

    //获取首页食品
    private List<PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponseListItem> getZhuanQuShiPin() {
        PddProductRecommendListRequest pddProductRecommendListRequest = new PddProductRecommendListRequest();
        pddProductRecommendListRequest.setChannelType(ProductChannelTypeEnum.CAINIXIHUAN.getCode());
        pddProductRecommendListRequest.setCatId(ProductGuessCategoryEnum.SHIPIN.getCode());
        pddProductRecommendListRequest.setLimit(6);
        PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponse productRecommend = pddProductService.getProductRecommend(pddProductRecommendListRequest);
        return productRecommend == null ? null : productRecommend.getList();
    }

    //获取首页母婴
    private List<PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponseListItem> getZhuanQuMuYing() {
        PddProductRecommendListRequest pddProductRecommendListRequest = new PddProductRecommendListRequest();
        pddProductRecommendListRequest.setChannelType(ProductChannelTypeEnum.CAINIXIHUAN.getCode());
        pddProductRecommendListRequest.setCatId(ProductGuessCategoryEnum.MUYING.getCode());
        pddProductRecommendListRequest.setLimit(6);
        PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponse productRecommend = pddProductService.getProductRecommend(pddProductRecommendListRequest);
        return productRecommend == null ? null : productRecommend.getList();
    }

    //获取首页女士精品
    private List<PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponseListItem> getZhuanQuNvZhuang() {
        PddProductRecommendListRequest pddProductRecommendListRequest = new PddProductRecommendListRequest();
        pddProductRecommendListRequest.setChannelType(ProductChannelTypeEnum.CAINIXIHUAN.getCode());
        pddProductRecommendListRequest.setCatId(ProductGuessCategoryEnum.NVZHUANG.getCode());
        pddProductRecommendListRequest.setLimit(6);
        PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponse productRecommend = pddProductService.getProductRecommend(pddProductRecommendListRequest);
        return productRecommend == null ? null : productRecommend.getList();
    }

    //获取首页美妆
    private List<PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponseListItem> getZhuanQuMeiZhuang() {
        PddProductRecommendListRequest pddProductRecommendListRequest = new PddProductRecommendListRequest();
        pddProductRecommendListRequest.setChannelType(ProductChannelTypeEnum.CAINIXIHUAN.getCode());
        pddProductRecommendListRequest.setCatId(ProductGuessCategoryEnum.MEIZHUANG.getCode());
        pddProductRecommendListRequest.setLimit(6);
        PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponse productRecommend = pddProductService.getProductRecommend(pddProductRecommendListRequest);
        return productRecommend == null ? null : productRecommend.getList();
    }

    //获取首页电器
    private List<PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponseListItem> getZhuanQuDianQi() {
        PddProductRecommendListRequest pddProductRecommendListRequest = new PddProductRecommendListRequest();
        pddProductRecommendListRequest.setChannelType(ProductChannelTypeEnum.CAINIXIHUAN.getCode());
        pddProductRecommendListRequest.setCatId(ProductGuessCategoryEnum.DIANQI.getCode());
        pddProductRecommendListRequest.setLimit(6);
        PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponse productRecommend = pddProductService.getProductRecommend(pddProductRecommendListRequest);
        return productRecommend == null ? null : productRecommend.getList();
    }

    //获取首页鞋包
    private List<PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponseListItem> getZhuanQuXieBao() {
        PddProductRecommendListRequest pddProductRecommendListRequest = new PddProductRecommendListRequest();
        pddProductRecommendListRequest.setChannelType(ProductChannelTypeEnum.CAINIXIHUAN.getCode());
        pddProductRecommendListRequest.setCatId(ProductGuessCategoryEnum.XIEBAO.getCode());
        pddProductRecommendListRequest.setLimit(6);
        PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponse productRecommend = pddProductService.getProductRecommend(pddProductRecommendListRequest);
        return productRecommend == null ? null : productRecommend.getList();
    }

    //获取首页内衣
    private List<PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponseListItem> getZhuanQuNeiYi() {
        PddProductRecommendListRequest pddProductRecommendListRequest = new PddProductRecommendListRequest();
        pddProductRecommendListRequest.setChannelType(ProductChannelTypeEnum.CAINIXIHUAN.getCode());
        pddProductRecommendListRequest.setCatId(ProductGuessCategoryEnum.NEIYI.getCode());
        pddProductRecommendListRequest.setLimit(6);
        PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponse productRecommend = pddProductService.getProductRecommend(pddProductRecommendListRequest);
        return productRecommend == null ? null : productRecommend.getList();
    }

    //获取首页水果
    private List<PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponseListItem> getZhuanQuShuiGuo() {
        PddProductRecommendListRequest pddProductSearchRequest = new PddProductRecommendListRequest();
        pddProductSearchRequest.setChannelType(ProductChannelTypeEnum.CAINIXIHUAN.getCode());
        pddProductSearchRequest.setCatId(ProductGuessCategoryEnum.SHUIGUO.getCode());
        pddProductSearchRequest.setLimit(6);
        PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponse productRecommend = pddProductService.getProductRecommend(pddProductSearchRequest);
        return productRecommend == null ? null : productRecommend.getList();
    }

    //获取首页精选爆品-官方直推爆款
    private List<PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponseListItem> getJingXuanBaoPin() {
        PddProductRecommendListRequest pddProductRecommendListRequest = new PddProductRecommendListRequest();
        List<Integer> listTag = new ArrayList<>();
        listTag.add(ProductActivityTagsEnum.JINGXUANBAOPINGF.getCode());
        pddProductRecommendListRequest.setActivityTags(listTag);
        pddProductRecommendListRequest.setLimit(6);
        PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponse productRecommend = pddProductService.getProductRecommend(pddProductRecommendListRequest);
        return productRecommend == null ? null : productRecommend.getList();
    }

}
