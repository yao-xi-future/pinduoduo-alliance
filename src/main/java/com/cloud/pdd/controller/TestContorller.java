package com.cloud.pdd.controller;

import com.cloud.common.util.R;
import com.cloud.pdd.jobhandler.XiatsOrderHandlers;
import com.cloud.pdd.service.PddCategoryService;
import com.pdd.pop.sdk.http.api.pop.response.PddGoodsCatsGetResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 阿里OSS内网映射
 * https://www.cnblogs.com/-mrl/p/10691271.html
 */
@Controller
@AllArgsConstructor
@RequestMapping("test")
public class TestContorller {
    private PddCategoryService pddCategoryService;
    private XiatsOrderHandlers xiatsOrderHandlers;
    @ResponseBody
    @RequestMapping("category")
    public R category() {
        //获取pdd所有分类
        List<PddGoodsCatsGetResponse.GoodsCatsGetResponseGoodsCatsListItem> allCategory = pddCategoryService.getAllCategory();
        return new R(allCategory);
    }

    @ResponseBody
    @RequestMapping("order")
    public R order() {
        xiatsOrderHandlers.main();
        return new R();
    }
}
