package com.cloud.pdd.controller;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cloud.pdd.model.domain.UserInfo;
import com.cloud.pdd.model.enums.PddMerchantTypeEnum;
import com.cloud.pdd.model.enums.ProducServiceTagsEnum;
import com.cloud.pdd.model.request.PddProductInfoRequest;
import com.cloud.pdd.model.request.PddProductSearchRequest;
import com.cloud.pdd.service.PddFavoriteService;
import com.cloud.pdd.service.PddProductService;
import com.cloud.pdd.service.PddUsersService;
import com.cloud.pdd.util.PddUtil;
import com.pdd.pop.sdk.http.api.pop.request.PddDdkGoodsPromotionUrlGenerateRequest;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkGoodsDetailResponse;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkGoodsPromotionUrlGenerateResponse;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkGoodsSearchResponse;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@AllArgsConstructor
@RequestMapping("product/info")
public class ProductInfoController extends BaseController {
    private final PddUsersService pddUsersService;
    private final PddProductService pddProductService;
    private final PddUtil pddUtil;
    private final PddFavoriteService pddFavoriteService;

    @GetMapping("")
    public ModelAndView info(HttpServletRequest request, HttpServletResponse response,
                             PddProductInfoRequest req) {
        UserInfo userInfo = pddUsersService.getUserInfo(request);
        PddDdkGoodsDetailResponse.GoodsDetailResponse productDetail = pddProductService.getProductDetail(req);
        List<PddDdkGoodsDetailResponse.GoodsDetailResponseGoodsDetailsItem> goodsList = productDetail.getGoodsDetails();
        PddDdkGoodsDetailResponse.GoodsDetailResponseGoodsDetailsItem goodsDetail = goodsList.stream().findFirst().orElse(null);
        if (goodsDetail == null) {
            return new ModelAndView("redirect:/");
        }
        //查询URL
//        getInfoUrl(goodsDetail,req.getSearchId());
        ModelAndView view = baseView(goodsDetail.getGoodsName(), "product/info");
        view.addObject("data", goodsDetail);
        view.addObject("req", req);
        view.addObject("pddUtil", pddUtil);
        view.addObject("minGroupPrice", goodsDetail.getMinGroupPrice() - goodsDetail.getCouponDiscount());
        view.addObject("minNormalPrice", goodsDetail.getMinNormalPrice() - goodsDetail.getCouponDiscount());
        view.addObject("serviceInfo", getProductService(goodsDetail.getServiceTags()));
        view.addObject("getMerchantType", getMerchantType(goodsDetail.getMerchantType()));
        BigDecimal realPayAmount = pddUtil.getProductCommission(goodsDetail.getPredictPromotionRate());
        view.addObject("realPayAmount", realPayAmount);
        view.addObject("isFavorite", isFavorite(userInfo, req));
        return view;
    }

    @GetMapping("convert")
    public ModelAndView convert(HttpServletRequest request, HttpServletResponse response,
                                PddProductInfoRequest req) {
        ModelAndView view = baseView("拼多多活动-商品转链", "product/convert");
        return view;
    }

    //是否收藏显示
    private boolean isFavorite(UserInfo userInfo, PddProductInfoRequest req) {
        if (userInfo == null) {
            return false;
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id", userInfo.getId());
        queryWrapper.eq("goods_id", req.getGoodsId());
        return pddFavoriteService.count(queryWrapper) > 0;
    }

    private void getInfoUrl(PddDdkGoodsDetailResponse.GoodsDetailResponseGoodsDetailsItem goodsDetail,
                            String searchId) {
        PddDdkGoodsPromotionUrlGenerateRequest pddDdkGoodsPromotionUrlGenerateRequest = new PddDdkGoodsPromotionUrlGenerateRequest();
        List<String> listGoodsSign = new ArrayList<>();
        listGoodsSign.add(goodsDetail.getGoodsSign());
        pddDdkGoodsPromotionUrlGenerateRequest.setGoodsSignList(listGoodsSign);
        pddDdkGoodsPromotionUrlGenerateRequest.setSearchId(searchId);
        PddDdkGoodsPromotionUrlGenerateResponse.GoodsPromotionUrlGenerateResponse productPromotionUrlGenerate = pddProductService.getProductPromotionUrlGenerate(pddDdkGoodsPromotionUrlGenerateRequest);
        List<PddDdkGoodsPromotionUrlGenerateResponse.GoodsPromotionUrlGenerateResponseGoodsPromotionUrlListItem> goodsPromotionUrlList = productPromotionUrlGenerate.getGoodsPromotionUrlList();

    }

    private String getMerchantType(Integer type) {
        for (Map<String, String> map : PddMerchantTypeEnum.getAllEnum()) {
            Integer code = Convert.toInt(map.get("code"));
            if (code == type) {
                return map.get("desc");
            }
        }
        return "";
    }

    private String getProductService(List<Integer> listTags) {
        if (listTags == null || listTags.size() <= 0) {
            return "";
        }
        List<Map<String, String>> allEnum = ProducServiceTagsEnum.getAllEnum();
        List<String> listTagName = new ArrayList<>();
        for (Integer tag : listTags) {
            Map<String, String> codeMap = allEnum.stream()
                    .filter(t -> Convert.toInt(t.get("code")) == tag).findFirst().orElse(null);
            if (codeMap == null) {
                continue;
            }
            String desc = codeMap.get("desc");
            listTagName.add(desc);
        }
        return StrUtil.join(",", listTagName);
    }
}
