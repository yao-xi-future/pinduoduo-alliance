package com.cloud.pdd.controller;

import com.cloud.pdd.config.Config;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@AllArgsConstructor
@RequestMapping("")
public class UserController {
    private final Config config;

    /**
     * 注册
     */
    @GetMapping("reg")
    public ModelAndView reg(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView view = new ModelAndView("user/reg");
        view.addObject("wwwSite", config.getWwwSite());
        return view;
    }

    /**
     * 登录
     */
    @GetMapping("login")
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView view = new ModelAndView("user/login");
        return view;
    }

    /**
     * 退出
     */
    @GetMapping("logout")
    public ModelAndView logout(HttpServletRequest request, HttpServletResponse response) {
        return new ModelAndView("redirect:/login");
    }
}
