package com.cloud.pdd.controller;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.cloud.pdd.model.domain.UserInfo;
import com.cloud.pdd.model.entity.*;
import com.cloud.pdd.model.request.PddCenterRecordstListRequest;
import com.cloud.pdd.model.request.PddCenterFavoriteListRequest;
import com.cloud.pdd.model.request.PddCenterOrderListRequest;
import com.cloud.pdd.model.request.PddCenterWithdrawListRequest;
import com.cloud.pdd.service.*;
import com.cloud.pdd.util.PddUtil;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping("center")
public class CenterController extends BaseController {
    private final PddUsersService pddUsersService;
    private final PddOrderService pddOrderService;
    private final PddFavoriteService pddFavoriteService;
    private final PddUserRecordsService pddUserRecordsService;
    private final PddFinancesService pddFinancesService;
    private final PddUserBankcardsService pddUserBankcardsService;
    private final PddWithdrawalOrdersService pddWithdrawalOrdersService;
    private final PddUtil pddUtil;

    @GetMapping("")
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView view = baseView("个人中心", "center/index");
        UserInfo userInfo = pddUsersService.getUserInfo(request);
        String beiAnUrl = pddUsersService.getBeiAnUrl(userInfo.getId());
        if (!StrUtil.isEmpty(beiAnUrl)) {
            return new ModelAndView("redirect:" + beiAnUrl);
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id", userInfo.getId());
        //订单数量
        int orderCount = pddOrderService.count(queryWrapper);
        view.addObject("orderCount", orderCount);
        //收藏数量
        int favoriteCount = pddFavoriteService.count(queryWrapper);
        view.addObject("favoriteCount", favoriteCount);
        //查询用户余额
        PddFinances pddFinances = pddFinancesService.getOne(queryWrapper);
        pddFinances = pddFinances == null ? new PddFinances() : pddFinances;
        view.addObject("pddFinances", pddFinances);

        PddUsers pddUsersServiceById = pddUsersService.getById(userInfo.getId());
        String headImg = pddUsersServiceById.getPhoto();
        if (StrUtil.isEmpty(headImg)) {
            headImg = "/images/default_head.png";
        }
        view.addObject("nickName", pddUsersServiceById.getNickName());
        view.addObject("headImg", headImg);
        return view;
    }

    //设置
    @GetMapping("userinfo")
    public ModelAndView userinfo(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView view = baseView("设置", "center/userinfo");
        UserInfo userInfo = pddUsersService.getUserInfo(request);
        PddUsers pddUsersServiceById = pddUsersService.getById(userInfo.getId());
        String headImg = pddUsersServiceById.getPhoto();
        if (StrUtil.isEmpty(headImg)) {
            headImg = "/images/default_head.png";
        }
        view.addObject("nickName", pddUsersServiceById.getNickName());
        view.addObject("headImg", headImg);
        return view;
    }

    //账户信息
    @GetMapping("userinfoperson")
    public ModelAndView userinfoPerson(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView view = baseView("账户信息", "center/userinfoperson");
        UserInfo userInfo = pddUsersService.getUserInfo(request);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("id", userInfo.getId());
        PddUsers pddUsers = pddUsersService.getOne(queryWrapper);
        if (pddUsers == null) {
            return new ModelAndView("redirect:/");
        }
        String headImg = pddUsers.getPhoto();
        if (StrUtil.isEmpty(headImg)) {
            headImg = "/images/default_head.png";
        }
        String head = "<div id=\"headimg\" style=\"background-image:url(" + headImg + ");\"></div>";
        view.addObject("data", pddUsers);
        view.addObject("head", head);
        return view;
    }

    //密码修改
    @GetMapping("userinfopwd")
    public ModelAndView userinfoMima(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView view = baseView("密码修改", "center/userinfopwd");

        return view;
    }

    //我的订单
    @GetMapping("order")
    public ModelAndView order(HttpServletRequest request, HttpServletResponse response,
                              PddCenterOrderListRequest req) {
        ModelAndView view = baseView("我的订单", "center/order");
        UserInfo userInfo = pddUsersService.getUserInfo(request);
        Page p = new Page();
        Integer currentPage = Convert.toInt(req.getPage(), 1);
        p.setCurrent(currentPage < 1 ? 1 : currentPage);
        p.setSize(req.getLimit() == null ? 10 : req.getLimit());
        req.setUserId(userInfo.getId());
        IPage<PddOrder> pddOrderIPage = pddOrderService.selectList(p, req);
        List<PddOrder> records = pddOrderIPage.getRecords();
        String orderNav = pddOrderService.getCenterOrderNav(req.getOrderStatus());
        view.addObject("orderNav", orderNav);
        view.addObject("data", records);
        view.addObject("page", pddUtil.getPageData(
                request,
                "/center/order",
                pddOrderIPage.getCurrent(),
                pddOrderIPage.getTotal(),
                pddOrderIPage.getSize()));
        view.addObject("pddUtil", pddUtil);
        return view;
    }

    //我的账单
    @GetMapping("records")
    public ModelAndView records(HttpServletRequest request, HttpServletResponse response,
                                PddCenterRecordstListRequest req) {
        ModelAndView view = baseView("我的账单", "center/records");
        UserInfo userInfo = pddUsersService.getUserInfo(request);
        Page p = new Page();
        Integer currentPage = Convert.toInt(req.getPage(), 1);
        p.setCurrent(currentPage < 1 ? 1 : currentPage);
        p.setSize(req.getLimit() == null ? 10 : req.getLimit());
        req.setUserId(userInfo.getId());
        IPage<PddUserRecords> pddUserRecordsIPage = pddUserRecordsService.selectList(p, req);
        List<PddUserRecords> records = pddUserRecordsIPage.getRecords();
        view.addObject("data", records);
        view.addObject("page", pddUtil.getPageData(
                request,
                "/center/records",
                pddUserRecordsIPage.getCurrent(),
                pddUserRecordsIPage.getTotal(),
                pddUserRecordsIPage.getSize()));
        view.addObject("pddUtil", pddUtil);
        return view;
    }

    //我的收藏
    @GetMapping("favorite")
    public ModelAndView favorite(HttpServletRequest request, HttpServletResponse response,
                                 PddCenterFavoriteListRequest req) {
        ModelAndView view = baseView("我的收藏", "center/favorite");
        UserInfo userInfo = pddUsersService.getUserInfo(request);
        Page p = new Page();
        Integer currentPage = Convert.toInt(req.getPage(), 1);
        p.setCurrent(currentPage < 1 ? 1 : currentPage);
        p.setSize(req.getLimit() == null ? 10 : req.getLimit());
        req.setUserId(userInfo.getId());
        IPage<PddFavorite> pddFavoriteIPage = pddFavoriteService.selectList(p);
        List<PddFavorite> records = pddFavoriteIPage.getRecords();
        view.addObject("data", records);
        view.addObject("page", pddUtil.getPageData(
                request,
                "/center/favorite",
                pddFavoriteIPage.getCurrent(),
                pddFavoriteIPage.getTotal(),
                pddFavoriteIPage.getSize()));
        view.addObject("pddUtil", pddUtil);
        return view;
    }

    //绑卡
    @GetMapping("bank")
    public ModelAndView bank(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView view = baseView("绑卡", "center/bank");
        UserInfo userInfo = pddUsersService.getUserInfo(request);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id", userInfo.getId());
        PddUserBankcards pddUserBankcards = pddUserBankcardsService.getOne(queryWrapper);
        view.addObject("pddUserBankcards", pddUserBankcards);
        return view;
    }

    //提现记录
    @GetMapping("withdraw")
    public ModelAndView withdraw(HttpServletRequest request, HttpServletResponse response,
                                 PddCenterWithdrawListRequest req) {
        ModelAndView view = baseView("提现记录", "center/withdraw");
        UserInfo userInfo = pddUsersService.getUserInfo(request);
        Page p = new Page();
        Integer currentPage = Convert.toInt(req.getPage(), 1);
        p.setCurrent(currentPage < 1 ? 1 : currentPage);
        p.setSize(req.getLimit() == null ? 10 : req.getLimit());
        req.setUserId(userInfo.getId());
        IPage<PddWithdrawalOrders> pddWithdrawalOrdersIPage = pddWithdrawalOrdersService.selectList(p, req);
        List<PddWithdrawalOrders> records = pddWithdrawalOrdersIPage.getRecords();
        view.addObject("data", records);
        view.addObject("page", pddUtil.getPageData(
                request,
                "/center/withdraw",
                pddWithdrawalOrdersIPage.getCurrent(),
                pddWithdrawalOrdersIPage.getTotal(),
                pddWithdrawalOrdersIPage.getSize()));
        view.addObject("pddUtil", pddUtil);
        return view;
    }

    //申请提现
    @SneakyThrows
    @GetMapping("apply_withdraw")
    public ModelAndView applyWithdraw(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView view = baseView("申请提现", "center/apply_withdraw");
        UserInfo userInfo = pddUsersService.getUserInfo(request);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("user_id", userInfo.getId());
        PddUserBankcards pddUserBankcards = pddUserBankcardsService.getOne(queryWrapper);
        if (pddUserBankcards == null) {
            response.getWriter().write("<script>alert('请绑卡后在申请提现');window.location='/center/bank';</script>");
        }
        view.addObject("pddUserBankcards", pddUserBankcards);
        view.addObject("minWithdraw", config.getMinWithdraw());

        //查询用户余额
        PddFinances pddFinances = pddFinancesService.getOne(queryWrapper);
        pddFinances = pddFinances == null ? new PddFinances() : pddFinances;
        view.addObject("money", pddFinances.getMoney() == null ? 0 : pddFinances.getMoney());

        return view;
    }

}

