package com.cloud.pdd.jobhandler;

import cn.hutool.http.HttpUtil;
import com.cloud.pdd.config.Config;
import com.cloud.pdd.service.PddOrderService;
import com.cloud.pdd.util.ApplicationContextProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 拉取订单数据
 * 窦海超
 * 2021年4月9日 15:20:00
 */
@Component
public class XiatsOrderHandlers {
    private static boolean isLock = true;
    @Autowired
    PddOrderService pddOrderService;

    public void main() {
        if (isLock) {
            isLock = false;
            try {
                Config configJob = ApplicationContextProvider.getBean(Config.class);
                HttpUtil.get(configJob.getWebSite() + "order/orderSync");
            } catch (Exception e) {
                System.out.print(e);
            } finally {
                isLock = true;
            }
//            pddOrderService.syncOrder(30);
        }
    }
}
