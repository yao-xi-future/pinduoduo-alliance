package com.cloud.pdd.model.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 提现订单
 *
 * @author douhaichao code generator
 * @date 2021-04-14 11:48:31
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("pdd_withdrawal_orders")
public class PddWithdrawalOrders extends Model<PddWithdrawalOrders> {

    /**
     *
     */
    @TableId
    private Integer id;

    /**
     * 用户UID
     */
    private Integer userId;

    /**
     * 金额
     */
    private BigDecimal cash;

    /**
     * 实际金额
     */
    private BigDecimal actualCash;

    /**
     * 单号
     */
    private String orderId;

    /**
     * 提现方式
     */
    private String way;

    /**
     * 账户姓名
     */
    private String accountName;

    /**
     * 账号
     */
    private String accountNumber;

    /**
     * 账户信息
     */
    private String accountData;

    /**
     * 平台单号
     */
    private String payOrderId;

    /**
     * 审核:3待审核，1审核通过，2拒绝
     */
    private Integer isCheck;

    /**
     * 审核时间
     */
    private Date checkTime;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;

}
