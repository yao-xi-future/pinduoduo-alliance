package com.cloud.pdd.model.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigInteger;
        import java.math.BigDecimal;
    import java.util.Date;

/**
 * 拼多多财务
 *
 * @author douhaichao code generator
 * @date 2021-04-14 13:47:29
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("pdd_finances")
public class PddFinances extends Model<PddFinances>  {

/**
 * 用户ID
 */
        @TableId
        private Integer userId;

/**
 * 现金
 */
        private BigDecimal money;

/**
 * 更新时间
 */
        private Date updateTime;


}
