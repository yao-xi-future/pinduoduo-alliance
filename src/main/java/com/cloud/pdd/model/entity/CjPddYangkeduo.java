package com.cloud.sh.model.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 采集拼多多商品
 *
 * @author douhaichao code generator
 * @date 2021-03-18 20:59:14
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("cj_pdd_yangkeduo")
public class CjPddYangkeduo extends Model<CjPddYangkeduo> {

    /**
     *
     */
    @TableId
    private Integer id;

    /**
     * 名称
     */
    private String title;

    /**
     * 图片
     */
    private String images;

    /**
     * 原价格
     */
    private BigDecimal originalPrice;

    /**
     * 现价格
     */
    private BigDecimal currentPrice;

    /**
     * 卖家门店
     */
    private String sellerName;

    /**
     * 描述
     */
    private String content;

    /**
     * 品牌
     */
    private String brand;

    /**
     * 药品名称
     */
    private String drugName;

    /**
     * 药品通用名称
     */
    private String commonDrugName;

    /**
     * 规格
     */
    private String specifications;

    /**
     * 剂型
     */
    private String dosage;

    /**
     * 剂量
     */
    private String dose;

    /**
     * 批准文号
     */
    private String approvalNumber;

    /**
     * 药品分类
     */
    private String category;

    /**
     * 有效期
     */
    private String validity;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     *
     */
    @TableLogic
    private Integer isDel;

    /**
     *
     */
    private Integer userId;

    private String leibie;
    private String shengchangqiye;
    private String yongfa;

}
