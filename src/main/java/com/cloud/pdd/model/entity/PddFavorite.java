package com.cloud.pdd.model.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 我的收藏
 *
 * @author douhaichao code generator
 * @date 2021-04-16 01:10:44
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("pdd_favorite")
public class PddFavorite extends Model<PddFavorite> {

    /**
     *
     */
    @TableId
    private Integer id;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     *
     */
    private String goodsImg;

    /**
     * 价格
     */
    private BigDecimal price;

    /**
     *
     */
    private Integer userId;

    /**
     *
     */
    @TableLogic
    private Integer isDel;

    /**
     *
     */
    private Date createTime;

    private String goodsId;

    private String searchId;

}
