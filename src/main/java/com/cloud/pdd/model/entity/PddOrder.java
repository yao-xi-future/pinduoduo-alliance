package com.cloud.pdd.model.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

/**
 * 拼多多订单
 *
 * @author douhaichao code generator
 * @date 2021-04-14 11:10:20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("pdd_order")
public class PddOrder extends Model<PddOrder> {

    /**
     *
     */
    @TableId
    private Integer id;

    /**
     * 订单ID
     */
    private String orderId;

    /**
     * 推广订单编号
     */
    private String orderSn;

    /**
     * 商品ID
     */
    private String goodsId;

    /**
     * 商品标题
     */
    private String goodsName;

    /**
     * 订单中sku的单件价格，单位为分
     */
    private String goodsPrice;

    /**
     * 购买商品的数量
     */
    private String goodsQuantity;

    /**
     *
     */
    private String goodsSign;

    /**
     * 商品缩略图
     */
    private String goodsThumbnailUrl;

    /**
     * 成团编号
     */
    private String groupId;

    /**
     * 实际支付金额，单位为分
     */
    private String orderAmount;

    /**
     * 订单生成时间，UNIX时间戳
     */
    private Date orderCreateTime;

    /**
     * 成团时间
     */
    private Date orderGroupSuccessTime;

    /**
     * 最后更新时间
     */
    private Date orderModifyAt;

    /**
     * 支付时间
     */
    private Date orderPayTime;

    /**
     * 确认收货时间
     */
    private Date orderReceiveTime;

    /**
     * 结算时间
     */
    private Date orderSettleTime;

    /**
     * 订单状态：0-已支付；1-已成团；2-确认收货；3-审核成功；4-审核失败（不可提现）；5-已经结算 ;10-已处罚
     */
    private Integer orderStatus;

    /**
     * 订单状态描述
     */
    private String orderStatusDesc;

    /**
     * 审核时间
     */
    private Date orderVerifyTime;

    /**
     * 佣金金额，单位为分
     */
    private String promotionAmount;

    /**
     * 佣金比例，千分比
     */
    private String promotionRate;

    /**
     * 推广位ID
     */
    private String pId;

    /**
     * 自定义参数
     */
    private String customParameters;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     *
     */
    @TableLogic
    private Integer isDel;

    /**
     * 订单审核失败原因
     */
    private String failReason;

    /**
     * 结算状态：0待结算 1已结算
     * */
    private Integer pddPayStatus;

    /**
     * 收入金额
     * */
    private BigDecimal userAmount;

}
