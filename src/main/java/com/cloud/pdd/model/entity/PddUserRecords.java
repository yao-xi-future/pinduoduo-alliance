package com.cloud.pdd.model.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigInteger;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 用户订单流水
 *
 * @author douhaichao code generator
 * @date 2021-04-14 11:48:33
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("pdd_user_records")
public class PddUserRecords extends Model<PddUserRecords> {

    /**
     *
     */
    @TableId
    private Integer id;

    /**
     * 用户ID
     */
    private Integer userId;

    /**
     * 收支
     */
    private BigDecimal income;

    /**
     * 余额
     */
    private BigDecimal surplus;

    /**
     *
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     *
     */
    @TableLogic
    private Integer isDel;

}
