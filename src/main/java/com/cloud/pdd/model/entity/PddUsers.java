package com.cloud.pdd.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigInteger;
import java.util.Date;

/**
 * 博客用户
 *
 * @author douhaichao code generator
 * @date 2021-02-25 17:54:12
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("pdd_users")
public class PddUsers extends Model<PddUsers> {

    /**
     *
     */
    @TableId
    private Integer id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 密码
     */
    private String password;

    /**
     * 手机号
     */
    private BigInteger mobile;

    /**
     * 注册IP
     */
    private String regip;

    /**
     * 注册时间
     */
    private Date regdate;

    /**
     * 最后登陆IP
     */
    private String lastloginip;

    /**
     * 最后登陆时间
     */
    private Date lastlogintime;

    /**
     * 介绍人
     */
    private Integer introducer;

    /**
     *
     */
    private Date createTime;

    /**
     *
     */
    private Date motifyTime;
    private String photo;
    private String nickName;

    private Integer sex;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    @TableField(exist = false)
    private String birthdayStr;
}
