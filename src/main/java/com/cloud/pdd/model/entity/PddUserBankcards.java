package com.cloud.pdd.model.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigInteger;
import java.util.Date;

/**
 * 用户卡号绑定
 *
 * @author douhaichao code generator
 * @date 2021-04-14 11:48:29
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@TableName("pdd_user_bankcards")
public class PddUserBankcards extends Model<PddUserBankcards> {

    /**
     *
     */
    @TableId
    private Integer id;

    /**
     * 用户UID
     */
    private Integer userId;

    /**
     * 账号类型：1支付宝，2微信
     */
    private String way;

    /**
     * 姓名
     */
    private String name;

    /**
     * 账号
     */
    private String number;

    /**
     * 账户信息
     */
    private String data;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 用户UID
     */
    @TableLogic
    private Integer isDel;

}
