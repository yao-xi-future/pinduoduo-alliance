package com.cloud.pdd.model.response;

import com.cloud.common.core.constant.CommonConstant;
import com.cloud.common.util.HttpReturnRnums;
import lombok.*;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data

@Builder
@ToString
@Accessors(chain = true)
@AllArgsConstructor
public class AjaxResp<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Getter
    @Setter
    private int code = CommonConstant.SUCCESS;

    @Getter
    @Setter
    private String msg = "success";

    @Getter
    @Setter
    private T data;

    @Getter
    @Setter
    private long count;

    public AjaxResp() {
        super();
    }

    public AjaxResp(T data) {
        super();
        this.data = data;
    }

    public AjaxResp(T data, String msg) {
        super();
        this.data = data;
        this.msg = msg;
    }

    public AjaxResp(int code, String msg) {
        super();
        this.code = code;
        this.msg = msg;
    }

    public AjaxResp(Throwable e) {
        super();
        this.msg = e.getMessage();
        this.code = CommonConstant.FAIL;
    }

    public AjaxResp(HttpReturnRnums httpReturnRnums) {
        super();
        this.msg = httpReturnRnums.desc();
        this.code = httpReturnRnums.value();
    }

    public AjaxResp(HttpReturnRnums httpReturnRnums, T data) {
        super();
        this.msg = httpReturnRnums.desc();
        this.code = httpReturnRnums.value();
        this.data = data;
    }

    public AjaxResp(HttpReturnRnums httpReturnRnums, T data, long count) {
        super();
        this.msg = httpReturnRnums.desc();
        this.code = httpReturnRnums.value();
        this.data = data;
        this.count = count;
    }
}
