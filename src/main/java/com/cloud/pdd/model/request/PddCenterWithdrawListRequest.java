package com.cloud.pdd.model.request;

import lombok.Data;

@Data
public class PddCenterWithdrawListRequest {
    private Integer page;
    private Integer limit;
    private Integer userId;
}
