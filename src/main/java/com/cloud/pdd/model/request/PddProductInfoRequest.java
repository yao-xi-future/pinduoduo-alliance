package com.cloud.pdd.model.request;

import lombok.Data;

@Data
public class PddProductInfoRequest {
    private String goodsId;
    private String searchId;
}

