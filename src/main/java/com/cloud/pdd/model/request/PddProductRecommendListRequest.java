package com.cloud.pdd.model.request;

import lombok.Data;

import java.util.List;

@Data
public class PddProductRecommendListRequest {

    //翻页时建议填写前页返回的list_id值
//    private String listId;
    //	一页请求数量；默认值 ： 20
    private Integer limit;
    //从多少位置开始请求；默认值 ： 0，offset需是limit的整数倍，仅支持整页翻页
//    private Integer offset;

    //    进宝频道推广商品: 1-今日销量榜,3-相似商品推荐,4-猜你喜欢(和进宝网站精选一致),5-实时热销榜,6-实时收益榜。默认值5
    private Integer channelType;

    //    活动商品标记数组，例：[4,7]，4-秒杀，7-百亿补贴，31-品牌黑标，10564-精选爆品-官方直推爆款，10584-精选爆品-团长推荐，24-品牌高佣，其他的值请忽略
    private List<Integer> activityTags;
    //    猜你喜欢场景的商品类目，20100-百货，20200-母婴，20300-食品，20400-女装，20500-电器，20600-鞋包，20700-内衣，20800-美妆，20900-男装，21000-水果，21100-家纺，21200-文具,21300-运动,21400-虚拟,21500-汽车,21600-家装,21700-家具,21800-医药;
    private Long catId;
}
