package com.cloud.pdd.model.request;

import lombok.Data;

@Data
public class PddProductPriceRequest {
    /**
     * 1拼团
     * 2直接购买
     * */
    private String type;
    private String goodsSign;
    private String searchId;
}
