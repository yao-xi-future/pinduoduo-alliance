package com.cloud.pdd.model.request;

import lombok.Data;

import java.math.BigInteger;

@Data
public class RegRequest {

    /**
     * 用户名
     */
    private String username;
    private BigInteger mobile;
    private String email;
    /**
     * 密码
     */
    private String password;
    private String passwordConfirm;
    /**
     * 校验码
     */
    private String code;
    private String nickName;
}
