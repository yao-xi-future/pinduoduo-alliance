package com.cloud.pdd.model.request;

import lombok.Data;

@Data
public class LoginRequest {

    /**
     * 用户名
     */
    private String mobile;
    /**
     * 密码
     */
    private String password;

    /**
     * 校验码
     */
    private String code;
}
