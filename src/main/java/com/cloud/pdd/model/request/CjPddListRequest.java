package com.cloud.pdd.model.request;

import lombok.Data;

@Data
public class CjPddListRequest {
    private String title;
    private String startTime;
    private String endTime;
    private Integer userId;
}
