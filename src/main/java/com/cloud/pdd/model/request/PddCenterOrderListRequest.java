package com.cloud.pdd.model.request;

import lombok.Data;

@Data
public class PddCenterOrderListRequest {
    private Integer page;
    private Integer limit;
    //订单状态
    private String orderStatus;
    private Integer userId;
}
