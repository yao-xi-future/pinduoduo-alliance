package com.cloud.pdd.model.request;

import lombok.Data;

@Data
public class PddOrderListRequest {
    private Integer page;
    private Integer pageSize;
//    最近90天内多多进宝商品订单更新时间--查询时间开始。
//    note：此时间为时间戳，指格林威治时间 1970 年01 月 01 日 00 时 00 分 00 秒
//    (北京时间 1970 年 01 月 01 日 08 时 00 分 00 秒)起至现在的总秒数
    private Long startUpdateTime;
//    查询结束时间，和开始时间相差不能超过24小时
    private Long endUpdateTime;

}
