package com.cloud.pdd.model.request;

import lombok.Data;

@Data
public class PddCenterUserPasswordRequest {
    private String oldPassword;
    private String newPassowrd;
    private String confirmNewPassword;
}
