package com.cloud.pdd.model.request;

import lombok.Data;

@Data
public class PasswordRequest {
    private String oldPassword;
    private String newPassword;
    private String newConfirmPassword;
}
