package com.cloud.pdd.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 产品服务
 * */
public enum ProducServiceTagsEnum {
    SONGHUORUHUBINGANZHUANG(4, "送货入户并安装"),
    DIANZIFAPIAO(6, "电子发票"),
    HUAIGUOBAOPEI(9, "坏果包赔"),
    SHANDIANTUIKUAN(11, "闪电退款"),
    ERSHISIXIAOSHIFAHUO(12, "24小时发货"),
    SISHIBAXIAOSHIFAHUO(13, "48小时发货"),
    SHUNFENGBAOYOU(17, "顺丰包邮"),
    ZHIHUANBUXIU(18, "只换不修"),
    KEDINGZHIHUA(1, "可定制化"),
    YUYUEPEISONG(29, "预约配送"),
    ZHENGPINFAPIAO(1000001, "正品发票"),
    SONGHUORUHUBINGANZHUANG1(1000002, "送货入户并安装"),
    ;

    private int code;
    private String desc;

    ProducServiceTagsEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static ProducServiceTagsEnum getEnum(String desc) {
        for (ProducServiceTagsEnum c : ProducServiceTagsEnum.values()) {
            if (c.getDesc().equals(desc)) {
                return c;
            }
        }
        return null;
    }

    public static ProducServiceTagsEnum getEnum(int index) {
        for (ProducServiceTagsEnum c : ProducServiceTagsEnum.values()) {
            if (c.getCode() == index) {
                return c;
            }
        }
        return null;
    }

    public static List<Map<String, String>> getAllEnum() {
        List<Map<String, String>> list = new ArrayList<>();
        for (ProducServiceTagsEnum c : ProducServiceTagsEnum.values()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("code", c.getCode() + "");
            map.put("desc", c.getDesc());
            list.add(map);
        }
        return list;
    }
}
