package com.cloud.pdd.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 店铺类型
 * */
public enum PddMerchantTypeEnum {
    GEREN(1, "个人"),
    QIYE(2, "企业"),
    QIJIANDIAN(3, "旗舰店"),
    ZHUANMAIDIAN(4, "专卖店"),
    ZHUANYINGDIAN(5, "专营店"),
    PUTONGDIAN(6, "普通店"),
    ;

    private int code;
    private String desc;

    PddMerchantTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static PddMerchantTypeEnum getEnum(String desc) {
        for (PddMerchantTypeEnum c : PddMerchantTypeEnum.values()) {
            if (c.getDesc().equals(desc)) {
                return c;
            }
        }
        return null;
    }

    public static PddMerchantTypeEnum getEnum(int index) {
        for (PddMerchantTypeEnum c : PddMerchantTypeEnum.values()) {
            if (c.getCode() == index) {
                return c;
            }
        }
        return null;
    }

    public static List<Map<String, String>> getAllEnum() {
        List<Map<String, String>> list = new ArrayList<>();
        for (PddMerchantTypeEnum c : PddMerchantTypeEnum.values()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("code", c.getCode() + "");
            map.put("desc", c.getDesc());
            list.add(map);
        }
        return list;
    }
}
