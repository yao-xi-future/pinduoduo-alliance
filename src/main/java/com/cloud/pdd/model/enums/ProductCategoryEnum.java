package com.cloud.pdd.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//查询矢量图：https://www.iconfont.cn
//首页导航,分类页，根据分类商品商品列表页
public enum ProductCategoryEnum {
    JINGXUAN(8569L, "精选", "/images/ico/jingxuan.png"),
    BAIHUO(8590L, "百货", "/images/ico/baihuo.png"),
    SHIPIN(8584L, "食品", "/images/ico/shipin.png"),
    MUYING(8587L, "母婴", "/images/ico/muying.png"),
    NVSHI(8572L, "女装/女士精品", "/images/ico/nvzhuang.png"),
    MEIZHUANG(8593L, "美妆", "/images/ico/meizhuang.png"),
    DIANQI(9019L, "电器", "/images/ico/dianqi.png"),
    XIEBAO(8605L, "鞋包", "/images/ico/xiebao.png"),
    NEIYI(8599L, "内衣", "/images/ico/neiyi.png"),
    NANZHUANG(8581L, "男装", "/images/ico/nanzhuang.png"),
    SHUIGUO(13L, "水果", "/images/ico/shuiguo.png"),
    HAITAO(12L, "海淘", "/images/ico/haitao.png"),
    YIYAO(3279L, "医药", "/images/ico/yiyao.png"),
    SHOUJI(9016L, "手机", "/images/ico/shouji.png"),
    CHONGZHI(590L, "充值", "/images/ico/chongzhi.png"),
    ;

    private Long code;
    private String desc;
    private String imgUrl;

    ProductCategoryEnum(Long code, String desc, String imgUrl) {
        this.code = code;
        this.desc = desc;
        this.imgUrl = imgUrl;
    }

    public Long getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public static ProductCategoryEnum getEnum(String desc) {
        for (ProductCategoryEnum c : ProductCategoryEnum.values()) {
            if (c.getDesc().equals(desc)) {
                return c;
            }
        }
        return null;
    }

    public static ProductCategoryEnum getEnum(int index) {
        for (ProductCategoryEnum c : ProductCategoryEnum.values()) {
            if (c.getCode() == index) {
                return c;
            }
        }
        return null;
    }

    public static List<Map<String, String>> getAllEnum() {
        List<Map<String, String>> list = new ArrayList<>();
        for (ProductCategoryEnum c : ProductCategoryEnum.values()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("code", c.getCode() + "");
            map.put("desc", c.getDesc());
            map.put("imgUrl", c.getImgUrl());
            list.add(map);
        }
        return list;
    }

}
