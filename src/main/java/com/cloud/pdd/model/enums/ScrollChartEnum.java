package com.cloud.pdd.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ScrollChartEnum {
    HOMESCROLL(1, "首页滚动"),
    HOMESCROLLRIGHT(2, "首页滚动右侧"),
    HOMECENTER(3, "首页中间"),
    RIGHTONE(4, "右一"),
    RIGHTTWO(5, "右二")
    ;
    private int code;
    private String desc;

    ScrollChartEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static ScrollChartEnum getEnum(String desc) {
        for (ScrollChartEnum c : ScrollChartEnum.values()) {
            if (c.getDesc().equals(desc)) {
                return c;
            }
        }
        return null;
    }

    public static ScrollChartEnum getEnum(int index) {
        for (ScrollChartEnum c : ScrollChartEnum.values()) {
            if (c.getCode() == index) {
                return c;
            }
        }
        return null;
    }

    public static List<Map<String, String>> getAllEnum() {
        List<Map<String, String>> list = new ArrayList<>();
        for (ScrollChartEnum c : ScrollChartEnum.values()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("value", c.getCode() + "");
            map.put("label", c.getDesc());
            list.add(map);
        }
        return list;
    }

}
