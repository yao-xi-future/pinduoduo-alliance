package com.cloud.pdd.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//猜你喜欢接口分类
//多多进宝商品推荐API
public enum ProductGuessCategoryEnum {
    BAIHUO(20100L,"百货",""),
    MUYING(20200L,"母婴",""),
    SHIPIN(20300L,"食品",""),
    NVZHUANG(20400L,"女装",""),
    DIANQI(20500L,"电器",""),
    XIEBAO(20600L,"鞋包",""),
    NEIYI(20700L,"内衣",""),
    MEIZHUANG(20800L,"美妆",""),
    NANZHUANG(20900L,"男装",""),
    SHUIGUO(21000L,"水果",""),
    JIAFANG(21100L,"家纺",""),
    WENJU(21200L,"文具",""),
    YUNDONG(21300L,"运动",""),
    XUNI(21400L,"虚拟",""),
    QICHE(21500L,"汽车",""),
    JIAZHUANG(21600L,"家装",""),
    JIAJU(21700L,"家具",""),
    YIYAO(21800L,"医药",""),
    ;

    private Long code;
    private String desc;
    private String imgUrl;

    ProductGuessCategoryEnum(Long code, String desc, String imgUrl) {
        this.code = code;
        this.desc = desc;
        this.imgUrl=imgUrl;
    }

    public Long getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public static ProductGuessCategoryEnum getEnum(String desc) {
        for (ProductGuessCategoryEnum c : ProductGuessCategoryEnum.values()) {
            if (c.getDesc().equals(desc)) {
                return c;
            }
        }
        return null;
    }

    public static ProductGuessCategoryEnum getEnum(int index) {
        for (ProductGuessCategoryEnum c : ProductGuessCategoryEnum.values()) {
            if (c.getCode() == index) {
                return c;
            }
        }
        return null;
    }

    public static List<Map<String, String>> getAllEnum() {
        List<Map<String, String>> list = new ArrayList<>();
        for (ProductGuessCategoryEnum c : ProductGuessCategoryEnum.values()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("code", c.getCode() + "");
            map.put("desc", c.getDesc());
            map.put("imgUrl", c.getImgUrl());
            list.add(map);
        }
        return list;
    }

}
