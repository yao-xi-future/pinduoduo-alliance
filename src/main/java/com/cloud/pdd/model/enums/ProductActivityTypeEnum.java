package com.cloud.pdd.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//暂未使用
public enum ProductActivityTypeEnum {
    NONE(0, "无活动"),
    MIAOSHA(1, "秒杀"),
    XIANLIANGZHEKOU(3, "限量折扣"),
    XIANSHIZHEKOU(12, "限时折扣"),
    DACUHUODONG(13, "大促活动"),
    MINGPINZHEKOU(14, "名品折扣"),
    PINPAIQINGCANG(15, "品牌清仓"),
    SHIPINCHAOSHI(16, "食品超市"),
    YIYUANXINGYUNTUAN(17, "一元幸运团"),
    AIGUANGJIE(18, "爱逛街"),
    SHISHANGCHUANDA(19, "时尚穿搭"),
    NANRENBANG(20, "男人帮"),
    JIUKUAIJIU(21, "9块9"),
    JINGJIAHUODONG(22, "竞价活动"),
    BANGDANHUODONG(23, "榜单活动"),
    XINGYUNBANJIAGOU(24, "幸运半价购"),
    DINGJINYUSHOU(25, "定金预售"),
    XINGYUNRENQIGOU(26, "幸运人气购"),
    TESEZHUTIHUODONG(27, "特色主题活动"),
    DUANMAQINGCANG(28, "断码清仓"),
    YIYUANHUAFEI(29, "一元话费"),
    DIANQICHENG(30, "电器城"),
    MEIRIHAODIAN(31, "每日好店"),
    PINPAIKA(33, "品牌卡"),
    DACUSOUSUOCHI(101, "大促搜索池"),
    DACUPINLEIFENHUICHANG(102, "大促品类分会场"),
    ;

    private int code;
    private String desc;

    ProductActivityTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static ProductActivityTypeEnum getEnum(String desc) {
        for (ProductActivityTypeEnum c : ProductActivityTypeEnum.values()) {
            if (c.getDesc().equals(desc)) {
                return c;
            }
        }
        return null;
    }

    public static ProductActivityTypeEnum getEnum(int index) {
        for (ProductActivityTypeEnum c : ProductActivityTypeEnum.values()) {
            if (c.getCode() == index) {
                return c;
            }
        }
        return null;
    }

    public static List<Map<String, String>> getAllEnum() {
        List<Map<String, String>> list = new ArrayList<>();
        for (ProductActivityTypeEnum c : ProductActivityTypeEnum.values()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("code", c.getCode() + "");
            map.put("desc", c.getDesc());
            list.add(map);
        }
        return list;
    }

}
