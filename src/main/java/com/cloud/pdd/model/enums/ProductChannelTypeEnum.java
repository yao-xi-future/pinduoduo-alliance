package com.cloud.pdd.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//进宝频道推广商品
public enum ProductChannelTypeEnum {
    JINRIXIAOLIANGBANG(1, "今日销量榜"),
    XIANGSISHANGPINTUIJIAN(3, "相似商品推荐"),
    CAINIXIHUAN(4, "猜你喜欢(和进宝网站精选一致)"),
    SHISHIREXIAOBANG(5, "实时热销榜"),
    SHISHISHOUYIBANG(6, "实时收益榜"),
    ;

    private int code;
    private String desc;

    ProductChannelTypeEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static ProductChannelTypeEnum getEnum(String desc) {
        for (ProductChannelTypeEnum c : ProductChannelTypeEnum.values()) {
            if (c.getDesc().equals(desc)) {
                return c;
            }
        }
        return null;
    }

    public static ProductChannelTypeEnum getEnum(int index) {
        for (ProductChannelTypeEnum c : ProductChannelTypeEnum.values()) {
            if (c.getCode() == index) {
                return c;
            }
        }
        return null;
    }

    public static List<Map<String, String>> getAllEnum() {
        List<Map<String, String>> list = new ArrayList<>();
        for (ProductChannelTypeEnum c : ProductChannelTypeEnum.values()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("code", c.getCode() + "");
            map.put("desc", c.getDesc());
            list.add(map);
        }
        return list;
    }

}
