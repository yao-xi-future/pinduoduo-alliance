package com.cloud.pdd.model.enums;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum ProductActivityTagsEnum {
    MIAOSHA(4, "秒杀"),
    BAIYIBUTIE(7, "百亿补贴"),
    PINPAIHEBIAO(31, "品牌黑标"),
    JINGXUANBAOPINGF(10564, "精选爆品-官方直推爆款"),
    JINGXUANBAOPINTZ(10584, "精选爆品-团长推荐"),
    PINPAIGAOYONG(24, "品牌高佣"),
    ;

    private int code;
    private String desc;

    ProductActivityTagsEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }

    public static ProductActivityTagsEnum getEnum(String desc) {
        for (ProductActivityTagsEnum c : ProductActivityTagsEnum.values()) {
            if (c.getDesc().equals(desc)) {
                return c;
            }
        }
        return null;
    }

    public static ProductActivityTagsEnum getEnum(int index) {
        for (ProductActivityTagsEnum c : ProductActivityTagsEnum.values()) {
            if (c.getCode() == index) {
                return c;
            }
        }
        return null;
    }

    public static List<Map<String, String>> getAllEnum() {
        List<Map<String, String>> list = new ArrayList<>();
        for (ProductActivityTagsEnum c : ProductActivityTagsEnum.values()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("code", c.getCode() + "");
            map.put("desc", c.getDesc());
            list.add(map);
        }
        return list;
    }
}
