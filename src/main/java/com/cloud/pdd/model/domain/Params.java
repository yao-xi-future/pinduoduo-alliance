package com.cloud.pdd.model.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * Created on 2019/7/26.
 *
 * @author
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Params {

    private String endTime;

    private String beginTime;

}
