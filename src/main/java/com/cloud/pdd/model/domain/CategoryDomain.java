package com.cloud.pdd.model.domain;

import lombok.Data;

@Data
public class CategoryDomain {
    private Integer cOneId;
    private Integer cTwoId;
    private String cOneName;
    private String cTwoName;
}
