package com.cloud.pdd.model.domain;

import lombok.Data;

@Data
public class ParamRequest {
    private Integer page;
    private Integer limit;
    private String searchParams;
}
