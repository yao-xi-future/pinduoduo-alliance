package com.cloud.pdd.model.domain;

import lombok.Data;

@Data
public class CjPddDomain {
    private String url;
    private String contents;
    private String imgUrl;
    private String key;
    private String values;
}
