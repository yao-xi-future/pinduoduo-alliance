package com.cloud.pdd.model.domain;

import lombok.Data;

import java.util.Date;

@Data
public class UserInfo {
    private Integer id;
    private String nickname;
    //    //头象
    private String photo;

    //登录时间
    private long loginTime;

}
