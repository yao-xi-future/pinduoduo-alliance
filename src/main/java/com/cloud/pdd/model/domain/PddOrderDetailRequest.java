package com.cloud.pdd.model.domain;

import lombok.Data;

@Data
public class PddOrderDetailRequest {
    //订单号
    private String orderSn;
    //订单类型：1-推广订单；2-直播间订单
    private Integer queryOrderType;
}
