package com.cloud.pdd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.pdd.model.entity.PddFinances;
import com.cloud.pdd.model.entity.PddWithdrawalOrders;
import org.springframework.stereotype.Repository;

/**
 * 拼多多财务
 *
 * @author douhaichao code generator
 * @date 2021-04-14 13:47:29
 */
@Repository
public interface PddFinancesMapper extends BaseMapper<PddFinances> {
    int applyWithdraw(PddWithdrawalOrders req);
}
