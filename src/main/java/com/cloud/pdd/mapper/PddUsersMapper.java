package com.cloud.pdd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.pdd.model.entity.PddUsers;
import org.springframework.stereotype.Repository;

/**
 * 电商用户
 *
 * @author douhaichao code generator
 * @date 2021-03-18 20:59:14
 */
@Repository
public interface PddUsersMapper extends BaseMapper<PddUsers> {

}
