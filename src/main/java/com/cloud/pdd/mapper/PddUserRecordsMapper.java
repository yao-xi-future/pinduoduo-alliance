package com.cloud.pdd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.pdd.model.entity.PddUserRecords;
import org.springframework.stereotype.Repository;

/**
 * 用户订单流水
 *
 * @author douhaichao code generator
 * @date 2021-04-14 11:48:33
 */
@Repository
public interface PddUserRecordsMapper extends BaseMapper<PddUserRecords> {

}
