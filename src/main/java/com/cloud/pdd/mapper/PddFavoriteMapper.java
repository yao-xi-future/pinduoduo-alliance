package com.cloud.pdd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.pdd.model.entity.PddFavorite;
import org.springframework.stereotype.Repository;

/**
 * 我的收藏
 *
 * @author douhaichao code generator
 * @date 2021-04-16 01:10:44
 */
@Repository
public interface PddFavoriteMapper extends BaseMapper<PddFavorite> {

}
