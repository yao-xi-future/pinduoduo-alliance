package com.cloud.pdd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.pdd.model.entity.PddWithdrawalOrders;
import org.springframework.stereotype.Repository;

/**
 * 提现订单
 *
 * @author douhaichao code generator
 * @date 2021-04-14 11:48:31
 */
@Repository
public interface PddWithdrawalOrdersMapper extends BaseMapper<PddWithdrawalOrders> {



}
