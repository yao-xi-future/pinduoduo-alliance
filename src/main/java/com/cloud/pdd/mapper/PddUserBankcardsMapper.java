package com.cloud.pdd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.pdd.model.entity.PddUserBankcards;
import org.springframework.stereotype.Repository;

/**
 * 用户卡号绑定
 *
 * @author douhaichao code generator
 * @date 2021-04-14 11:48:29
 */
@Repository
public interface PddUserBankcardsMapper extends BaseMapper<PddUserBankcards> {

}
