package com.cloud.pdd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.sh.model.entity.CjPddYangkeduo;
import org.springframework.stereotype.Repository;

/**
 * 采集拼多多商品
 *
 * @author douhaichao code generator
 * @date 2021-03-18 20:59:14
 */
@Repository
public interface CjPddYangkeduoMapper extends BaseMapper<CjPddYangkeduo> {

}
