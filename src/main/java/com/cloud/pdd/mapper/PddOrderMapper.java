package com.cloud.pdd.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cloud.pdd.model.entity.PddOrder;
import org.springframework.stereotype.Repository;

/**
 * 拼多多订单
 *
 * @author douhaichao code generator
 * @date 2021-04-14 11:10:20
 */
@Repository
public interface PddOrderMapper extends BaseMapper<PddOrder> {
//    boolean updateOrderSPddPayStatus(Integer id)
}
