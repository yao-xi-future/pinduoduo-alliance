package com.cloud.pdd.util;

import cn.hutool.core.util.StrUtil;
import com.cloud.pdd.config.Config;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class BlogUtils {

    private final Config config;

    /**
     * 获取图片地址
     */
    public String getListImgUrl(String imgUrl) {
        if (imgUrl.contains("http://") || imgUrl.contains("https://")) {
            return imgUrl;
        }
        return config.getImgUrl() + imgUrl + "_200x140.jpg";
    }

    /**
     * 获取第一张图片
     * */
    public String getFirstImgUrl(String imgUrl) {
        if (StrUtil.isEmpty(imgUrl)) {
            return "";
        }
        if (StrUtil.contains(imgUrl, ",")) {
            imgUrl = StrUtil.subBefore(imgUrl, ",", false);
        }
        return getListImgUrl(imgUrl);
    }
}
