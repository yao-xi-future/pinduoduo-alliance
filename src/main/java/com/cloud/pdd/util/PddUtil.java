package com.cloud.pdd.util;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.cloud.common.security.AES;
import com.cloud.common.util.CookieUtils;
import com.cloud.pdd.config.Config;
import com.cloud.pdd.model.domain.UserInfo;
import com.cloud.pdd.model.enums.CookieConst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

@Service
public class PddUtil {
    @Autowired
    Config config;

    //拼多多给的价格是分单位，需要除100
    public BigDecimal getPrice(Long singlePrice) {
        if (singlePrice == null) {
            return BigDecimal.ZERO;
        }
        BigDecimal div = NumberUtil.div(singlePrice, Convert.toBigDecimal(100), 2);
        return div;
    }

    public String getListUrl(Integer optId) {
        return "/product/list?optId=" + optId;
    }

    public String getDetailUrl(String goodsSign, String searchId) {
        return "/product/info?goodsId=" + goodsSign + "&searchId=" + searchId;
    }

    public String getCustomParameters() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String cookie = CookieUtils.getCookie(request, CookieConst.loginPddCookie);
        String userId = null;
        if (!StrUtil.isEmpty(cookie)) {
            String s = AES.aesDecrypt(cookie);
            UserInfo userInfo = JSONUtil.toBean(s, UserInfo.class);
            userId = userInfo.getId() + "";
        } else {
            //如果没登录就获取默认的uid
            userId = config.getPddUid();
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("uid", userId);
        return jsonObject.toString();
    }

    //通过实际佣金计算预计佣金
    public BigDecimal getProductCommission(Long promotionAmount) {
        if (promotionAmount == null) {
            return BigDecimal.ZERO;
        }
        BigDecimal incomeRate = config.getIncomeRate();
        BigDecimal rate = NumberUtil.mul(incomeRate, 0.01);
        //真实付款金额
        BigDecimal realPayAmount = NumberUtil.mul(promotionAmount, rate);
        return NumberUtil.div(realPayAmount, 100, 2);
    }

    public String getPageData(
            HttpServletRequest request,
            String jumpUrl,
            Long currentPage,
            Long total,
            Long pageSize) {
        Long maxPage = total / pageSize > 0 ? total / pageSize + 1 : total / pageSize;
        if (maxPage == 0 && total > 0) {
            maxPage = 1L;
        }
        String orderStatus = request.getParameter("orderStatus");
        orderStatus = StrUtil.isEmpty(orderStatus) ? "" : "orderStatus=" + orderStatus + "&";
        StringBuilder sb = new StringBuilder();
        if (currentPage > 1) {
            sb.append("<a href=\"" + jumpUrl + "?" + orderStatus + "page=" + (currentPage - 1) + "\">上一页</a>");
        }
        sb.append("<span>");
        sb.append("<em>" + currentPage + "</em>/<i>" + maxPage + "</i>");
        sb.append("<select>");
        for (int i = 1; i <= maxPage; i++) {
            sb.append("<option>第" + i + "页</option>");
        }
        sb.append("</select>");
        sb.append("</span>");
        if (currentPage < maxPage) {
            sb.append("<a href=\"" + jumpUrl + "?" + orderStatus + "page=" + (currentPage + 1) + "\">下一页</a>");
        }
        return sb.toString();
    }

    //获取OSS上传路径
    public String createOssFiledir(String fileType, String staff) {
        return fileType + "/" + DateUtil.today().replace("-", "/") + "/" + UUID.randomUUID().toString() + "." + staff;
    }
}
