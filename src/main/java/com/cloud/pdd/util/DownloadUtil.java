package com.cloud.pdd.util;
import javax.net.ssl.*;
import java.io.*;
import java.net.URL;
import java.security.SecureRandom;

public class DownloadUtil {

    /**
     * @param requestUrl https 远程路径
     * @param fileLocal  本地文件存放路径,需要注意的是这里是要传一个已经存在的文件，否则会报拒绝访问的的错误
     * @throws Exception
     */
    public static void downloadFile(String requestUrl, String fileLocal) throws Exception {
        try {
            BufferedReader bufferedReader = null;
            InputStreamReader inputStreamReader = null;
            InputStream inputStream = null;
            HttpsURLConnection httpUrlConn = null;
            // 建立并向网页发送请求
            try {
                TrustManager[] tm = {new MyX509TrustManager()};
                SSLContext sslContext = SSLContext.getInstance("SSL", "SunJSSE");
                sslContext.init(null, tm, new SecureRandom());
                // 从上述SSLContext对象中得到SSLSocketFactory对象
                SSLSocketFactory ssf = sslContext.getSocketFactory();
                URL url = new URL(requestUrl);
                // 描述状态
                httpUrlConn = (HttpsURLConnection) url.openConnection();
                httpUrlConn.setSSLSocketFactory(ssf);
                httpUrlConn
                        .setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36)");
                //防止报403错误。
                httpUrlConn.setDoOutput(true);
                httpUrlConn.setDoInput(true);
                httpUrlConn.setUseCaches(false);
                // 请求的类型
                httpUrlConn.setRequestMethod("GET");
                // 获取输入流
                inputStream = httpUrlConn.getInputStream();
                DataOutputStream out = new DataOutputStream(new FileOutputStream(fileLocal));
                byte[] buffer = new byte[inputStream.available()];
                int count = 0;
                while ((count = inputStream.read(buffer)) > 0) {
                    out.write(buffer, 0, count);
                }
                out.close();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                // 释放资源
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (inputStreamReader != null) {
                    try {
                        inputStreamReader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (httpUrlConn != null) {
                    httpUrlConn.disconnect();
                }
            }
        } catch (Exception e) {
            System.out.println(e);
//            BCommentServiceImpl bCommentService = new BCommentServiceImpl();
//            bCommentService.save(new BComment().setContent(requestUrl));
        } finally {
        }
    }

}
