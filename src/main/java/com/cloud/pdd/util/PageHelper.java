package com.cloud.pdd.util;

import com.baomidou.mybatisplus.core.metadata.IPage;

import java.util.List;

/**
 * @author douhaichao
 * @since 2020年12月23日 18:03:32
 */
public class PageHelper {
    // 总共的数据量
    private long total;

    // 每页显示多少条
    private long pageSize;

    // 共有多少页
    private long totalPage;

    // 当前是第几页
    private long index;

    // 数据
    private List<?> data;

    // 连接路径
    private String path = "";

    /**
     * 页码HTML信息
     */
    @SuppressWarnings("unused")
    private String pageHTML;

    private long startPage; // 开始页面

    private long endPage; // 结束页面

    private long displayNum = 3; // 显示的页数

    /**
     * @return the startPage
     */
    private long getStartPage() {
        return startPage;
    }

    /**
     * @return the endPage
     */
    private long getEndPage() {
        return endPage;
    }

    /**
     * @return the path
     */
    private String getPath() {
        return path;
    }

    private void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    private void setIndex(long index) {
        this.index = index;
    }

    /**
     * 设置路径前缀，页面第一页index为1
     *
     * @param path 此path含有参数形式，如/aa/article?page=,或者/bb/article/list/
     */
    private void setPath(String path) {
        this.path = path;
    }

    private long getPageSize() {
        return pageSize;
    }

    private long getTotalPage() {
        return (this.total + this.pageSize - 1) / this.pageSize;
    }

    private long getIndex() {
        return index;
    }

    private List<?> getData() {
        return data;
    }

    private void setData(List<?> data) {
        this.data = data;
    }

    /**
     * @return the total
     */
    private long getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    private void setTotal(long total) {
        this.total = total;
    }

    private String getPageHTML() {
        totalPage = getTotalPage();
        StringBuffer displayInfo = new StringBuffer();
        if (totalPage != 0 && pageSize != 0) {
            displayInfo.append("<div class='page-nav'>");
            // 判断如果当前是第一页 则“首页”和“第一页”失去链接
            if (index <= 1) {
                displayInfo.append("<a class='disabled'>首页</a>");
                displayInfo.append("<a class='disabled'>上一页</a>");
            } else {
                displayInfo.append("<a href='" + path + "1'>首页</a>");
                displayInfo.append("<a href='" + path + (index - 1) + "'>上一页</a>");
            }
            countPages();
            for (long i = startPage; i <= endPage; i++) {
                if (i == index) {
                    displayInfo.append("<span  style='cursor:pointer' class='current'>" + i + "</span>");
                } else {
                    displayInfo.append("<a href='" + path + i + "'>" + i + "</a>");
                }
            }
            if (index >= totalPage) {
                displayInfo.append("<a class='disabled'>下一页</a>");
                displayInfo.append("<a class='disabled'>尾页</a>");
            } else {
                displayInfo.append("<a href='" + path + (index + 1) + "'>下一页</a>");
                displayInfo.append("<a href='" + path + totalPage + "'>尾页</a>");
            }
            displayInfo.append(" 共 " + totalPage + " 页");
            displayInfo.append("</div>");
        }
        return displayInfo.toString();
    }

    private String getManagerPage() {
        totalPage = getTotalPage();
        StringBuffer displayInfo = new StringBuffer();
        if (totalPage <= 0) {
            return "";
        }
        String url = getPath();
        if (url.contains("?")) {
            url += "&";
        } else {
            url += "?";
        }
        displayInfo.append("<script>function goto(index){if(index==-1){index=document.getElementById(\"txtgo\").value;}\n" +
                "if(index>" + totalPage + "){alert(\"不能大于最大页数\");return;}\n" +
                "window.location.href=\"" + url + "current=\"+index;}</script>");
//        displayInfo.append("<script>function goto(index){if(index==-1){index=document.getElementById('txtgo').value;)}if(index>" + totalPage + "){alert('不能大于最大页数');return}window.location.href='" + getPath() + "?current='+index;}</script>");
        displayInfo.append("<div class=\"pageWrap\">\n" +
                "        <div class=\"table-footer\" style=\"background:#fff; padding:10px;\"><p class=\"pagRecord pagRecord1 fl\">");

        displayInfo.append("当前<input id=\"txtgo\" value=\"" + getIndex() + "\"\n" +
                "                   onkeyup=\"this.value=this.value.replace(/D/g,'')\"\n" +
                "                   onafterpaste=\"this.value=this.value.replace(/D/g,'')\"\n" +
                "                   class=\"pag-txt\" type=\"text\" value='" + index + "'>页" +
                "<input type=\"button\" onclick='goto(-1)' class=\"pag-btn\" value=\"确定\"></span>");

        displayInfo.append(" 共<span>" + totalPage + "</span>页<span class=\"ml-10\"> 跳到");

        displayInfo.append("<ul class=\"pagination fl\">");

//        displayInfo.append("当前 " + index + "页 共 " + totalPage + " 页，共 " + total + " 条</div></div>" +
//                "<div class=\"col-sm-6\"><div class=\"dataTables_paginate paging_simple_numbers\" id=\"DataTables_Table_0_paginate\">");
//        displayInfo.append("<ul class=\"pagination\">");
        // 判断如果当前是第一页 则“首页”和“第一页”失去链接
        String backMethod = "void(0)";
        String firstMethod = "void(0)";
        if (index > 1) {
            backMethod = "goto(" + (index - 1) + ")";
            firstMethod = "goto(1)";
        }
        displayInfo.append("<li class=\"paginate_button previous disabled\" aria-controls=\"editable\" tabindex=\"0\"><a\n" +
                "                        href=\"javascript:" + firstMethod + ";\">首页</a></li>\n" +
                "                <li class=\"paginate_button previous disabled\" aria-controls=\"editable\" tabindex=\"0\"><a\n" +
                "                        href=\"javascript:" + backMethod + ";\">上一页</a></li>");
        countPages();
        for (long i = startPage; i <= endPage; i++) {
            String active = "";
            if (i == index) {
                active = "active";
            }
            displayInfo.append("<li class=\"paginate_button " + active + "\" aria-controls=\"editable\" tabindex=\"0\"><a\n" +
                    "                        href=\"javascript:goto(" + (i) + ");\">" + (i) + "</a></li>");
        }
        String nextMethod = "void(0)";
        String endMethod = "void(0)";
        if (index < totalPage) {
            nextMethod = "goto(" + (index + 1) + ")";
            endMethod = "goto(" + totalPage + ")";
        }
        displayInfo.append("<li class=\"paginate_button next\" aria-controls=\"editable\" tabindex=\"0\"><a\n" +
                "                        href=\"javascript:" + nextMethod + ";\">下一页</a></li>\n" +
                "                <li class=\"paginate_button next\" aria-controls=\"editable\" tabindex=\"0\"><a\n" +
                "                        href=\"javascript:" + endMethod + ";\">末页</a></li>");

        displayInfo.append("</ul>");
        displayInfo.append("<p class=\"pagRecord pagRecord2 fl\">总共<span>" + getTotal() + "</span>条记录</p>\n" +
                "            <div class=\"clear\"></div>");
        displayInfo.append("</div></div>");
        return displayInfo.toString();
    }

    private String getPageNum(long pageindex) {
        return path.replace("##", String.valueOf(pageindex)).replace("#@", String.valueOf(pageindex));
    }

    private String getAjaxDYXPManagerPage() {
        totalPage = getTotalPage();
        StringBuffer displayInfo = new StringBuffer();
        if (totalPage != 0 && pageSize != 0) {
            displayInfo
                    .append("<div class=\"pageWrap\"><div class=\"table-footer\" style=\"background:#fff; padding:10px;\">");
            displayInfo.append("<p class=\"pagRecord pagRecord1 fl\">共<span>"
                    + totalPage + "</span>页");
            displayInfo.append("<span class=\"ml-10\"> 跳到");
            displayInfo.append("<input id=\"pagesearchvalue\" value=\"" + index
                    + "\" ");
            displayInfo
                    .append("onkeyup=\"this.value=this.value.replace(/\\D/g,'')\" ");
            displayInfo
                    .append("onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" ");
            displayInfo.append("class=\"pag-txt\" type=\"text\">页");
            displayInfo
                    .append("<input type=\"button\" id=\"pagesearch\" class=\"pag-btn\" value=\"确定\"></span>");

            //跳转到指定页
            displayInfo.append("<input type=\"hidden\" id=\"pagesearchcurrentpage\" value=\"" + index + "\">");
            displayInfo.append("<input type=\"hidden\" id=\"pagesearchmax\" value=\"" + totalPage + "\">");

            displayInfo.append("<ul class=\"pagination fl\">");
            // 判断如果当前是第一页 则“首页”和“第一页”失去链接
            if (index <= 1) {
                displayInfo
                        .append("<li class=\"paginate_button previous disabled\" aria-controls=\"editable\" tabindex=\"0\"><a href=\"javascript:void(0);\">首页</a></li>");
                displayInfo
                        .append("<li class=\"paginate_button previous disabled\" aria-controls=\"editable\" tabindex=\"0\"><a href=\"javascript:void(0);\">上一页</a></li>");
            } else {
                displayInfo
                        .append("<li class=\"paginate_button previous disabled\" aria-controls=\"editable\" tabindex=\"0\"><a href=\""
                                + getPageNum(1) + "\">首页</a></li>");
                displayInfo
                        .append("<li class=\"paginate_button previous disabled\" aria-controls=\"editable\" tabindex=\"0\"><a href=\""
                                + getPageNum(index - 1) + "\">上一页</a></li>");
            }
            countPages();
            for (long i = startPage; i <= endPage; i++) {
                if (i == index) {
                    displayInfo
                            .append("<li class=\"paginate_button active\" aria-controls=\"editable\" tabindex=\"0\"><a href=\"javascript:void(0);\">"
                                    + i + "</a></li>");
                } else {
                    displayInfo
                            .append("<li class=\"paginate_button\" aria-controls=\"editable\" tabindex=\"0\"><a href=\""
                                    + getPageNum(i) + "\">" + i + "</a></li>");
                }
            }
            if (index >= totalPage) {
                displayInfo
                        .append("<li class=\"paginate_button next\" aria-controls=\"editable\" tabindex=\"0\"><a href=\"javascript:void(0);\">下一页</a></li>");
                displayInfo
                        .append("<li class=\"paginate_button next\" aria-controls=\"editable\" tabindex=\"0\"><a href=\"javascript:void(0);\">末页</a></li>");
            } else {
                displayInfo
                        .append("<li class=\"paginate_button next\" aria-controls=\"editable\" tabindex=\"0\"><a href=\""
                                + getPageNum(index + 1) + "\">下一页</a></li>");
                displayInfo
                        .append("<li class=\"paginate_button next\" aria-controls=\"editable\" tabindex=\"0\"><a href=\""
                                + getPageNum(totalPage) + "\">末页</a></li>");
            }
            displayInfo.append("</ul>");
            displayInfo.append("<p class=\"pagRecord pagRecord2 fl\">总共<span>"
                    + total
                    + "</span>条记录" +
//                    "<span class=\"ml-10\">" +
//                    "每页显示<select class=\"mr-5 ml-5\" style=\"width:50px\" id=\"pagesearchpagesize\">");
//			displayInfo.append( "<option value='20' "+(pageSize==20?"selected=\"selected\"":"")+">20</option>");
//			displayInfo.append("<option value='50' "+(pageSize==50?"selected=\"selected\"":"")+">50</option>");
//			displayInfo.append("<option value='100' "+(pageSize==100?"selected=\"selected\"":"")+">100</option>");
//			displayInfo.append("<option value='200' "+(pageSize==200?"selected=\"selected\"":"")+">200</option>");
//			displayInfo.append("</select>条记录</span>" +
                    "</p>");
            displayInfo.append("<div class=\"clear\"></div></div></div>");
        }
        return displayInfo.toString();
    }

    private String getAjaxDYXPManagerPageNew() {
        totalPage = getTotalPage();
        StringBuffer displayInfo = new StringBuffer();
        if (totalPage != 0 && pageSize != 0) {
            displayInfo
                    .append("<div class=\"layui-box layui-laypage layui-laypage-default\">");
            displayInfo.append("<span class=\"layui-laypage-count\">共"
                    + totalPage + "页</span>");
            displayInfo.append("<span class=\"layui-laypage-skip\"> 跳到");
            displayInfo.append("<input id=\"pagesearchvalue\" value=\"" + index
                    + "\" ");
            displayInfo
                    .append("onkeyup=\"this.value=this.value.replace(/\\D/g,'')\" ");
            displayInfo
                    .append("onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" ");
            displayInfo.append("class=\"layui-input\" type=\"text\">页");
            displayInfo
                    .append("<input type=\"button\" id=\"pagesearch\" class=\"layui-laypage-btn\" value=\"确定\"></span>");

            //跳转到指定页
            displayInfo.append("<input type=\"hidden\" id=\"pagesearchcurrentpage\" value=\"" + index + "\">");
            displayInfo.append("<input type=\"hidden\" id=\"pagesearchmax\" value=\"" + totalPage + "\">");

            // 判断如果当前是第一页 则“首页”和“第一页”失去链接
            if (index <= 1) {
                displayInfo
                        .append("<a href=\"javascript:void(0);\" class=\"layui-laypage-prev disabled\" aria-controls=\"editable\" tabindex=\"0\">首页</a>");
                displayInfo
                        .append("<a href=\"javascript:void(0);\" class=\"layui-laypage-prev disabled\" aria-controls=\"editable\" tabindex=\"0\">上一页</a>");
            } else {
                displayInfo
                        .append("<a href=\""
                                + getPageNum(1) + "\" class=\"layui-laypage-prev disabled\" aria-controls=\"editable\" tabindex=\"0\">首页</a>");
                displayInfo
                        .append("<a href=\""
                                + getPageNum(index - 1) + "\" class=\"layui-laypage-prev disabled\" aria-controls=\"editable\" tabindex=\"0\">上一页</a>");
            }
            countPages();
            for (long i = startPage; i <= endPage; i++) {
                if (i == index) {

                    displayInfo.append("<span class=\"layui-laypage-curr\"><em class=\"layui-laypage-em\"></em><em>" + i + "</em></span>");
                    /*displayInfo.append("<a href=\"javascript:void(0);\" class=\"paginate_button active\" aria-controls=\"editable\" tabindex=\"0\">"
                                    + i + "</a>");*/
                } else {
                    displayInfo
                            .append("<a href=\""
                                    + getPageNum(i) + "\" class=\"paginate_button\" aria-controls=\"editable\" tabindex=\"0\">" + i + "</a>");
                }
            }
            if (index >= totalPage) {
                displayInfo
                        .append("<a href=\"javascript:void(0);\" class=\"paginate_button next\" aria-controls=\"editable\" tabindex=\"0\">下一页</a>");
                displayInfo
                        .append("<a href=\"javascript:void(0);\" class=\"paginate_button next\" aria-controls=\"editable\" tabindex=\"0\">末页</a>");
            } else {
                displayInfo
                        .append("<a href=\""
                                + getPageNum(index + 1) + "\" class=\"paginate_button next\" aria-controls=\"editable\" tabindex=\"0\">下一页</a>");
                displayInfo
                        .append("<a href=\""
                                + getPageNum(totalPage) + "\" class=\"paginate_button next\" aria-controls=\"editable\" tabindex=\"0\">末页</a>");
            }

            //下拉选择
            displayInfo.append("    <span class=\"layui-laypage-limits\">\n" +
                    "            <select lay-ignore=\"\" id=\"pagesearchpagesize\">\n" +
                    "              <option value='20' " + (pageSize == 20 ? "selected=\"selected\"" : "") + ">20 条/页</option>\n" +
                    "              <option value='50' " + (pageSize == 50 ? "selected=\"selected\"" : "") + ">50 条/页</option>\n" +
                    "              <option value='100' " + (pageSize == 100 ? "selected=\"selected\"" : "") + ">100 条/页</option>\n" +
                    "              <option value='200' " + (pageSize == 200 ? "selected=\"selected\"" : "") + ">200 条/页</option>\n" +
                    "            </select>\n" +
                    "            </span>");

            displayInfo.append("<span class=\"layui-laypage-count\">总共"
                    + total + "条记录</span>");
            displayInfo.append("<div class=\"clear\"></div></div>");
        }
        return displayInfo.toString();
    }

    private String getAjaxManagerPage() {

        totalPage = getTotalPage();
        StringBuffer displayInfo = new StringBuffer();
        if (totalPage != 0 && pageSize != 0) {
            displayInfo.append("<div class=\"pageWrap\">");
            displayInfo.append("<div class=\"turn_page clearfix\">");
            displayInfo.append("<span class=\"page_word\" style=\"font-weight:normal\">");
            displayInfo.append("共" + totalPage + "页," + total + "条记录,当前为第" + index + "页,每页" + pageSize + "条");
            displayInfo.append("</span>");
            // 判断如果当前是第一页 则“首页”和“第一页”失去链接
            if (index <= 1) {
                displayInfo.append("<a href=\"javascript:void(0);\" class=\"page_prev\">首页</a>");
                displayInfo.append("<a href=\"javascript:void(0);\" class=\"page_prev\">上一页</a>");
            } else {
                displayInfo.append("<a href=\"" + getPageNum(1) + "\" class=\"page_prev\">首页</a>");
                displayInfo.append("<a href=\"" + getPageNum(index - 1) + "\" class=\"page_prev\"'>上一页</a>");
            }
            countPages();
            for (long i = startPage; i <= endPage; i++) {
                if (i == index) {

                    displayInfo.append("<span class=\"page_cur\">" + i + "</span>");
                    //displayInfo.append(" <a href=\"javascript:void(0);\">" + i + "</a>");
                } else {
                    displayInfo.append("<a href='" + getPageNum(i) + "'>" + i + "</a>");
                }
            }
            if (index >= totalPage) {
                displayInfo.append("<a href=\"javascript:void(0);\" class=\"page_next\">下一页</a>");
                displayInfo.append("<a href=\"javascript:void(0);\" class=\"page_next\">末页</a>");
            } else {
                displayInfo.append("<a href='" + getPageNum(index + 1) + "' class=\"page_next\">下一页 </a></li>");
                displayInfo.append("<a href='" + getPageNum(totalPage) + "' class=\"page_next\">末页 </a></li>");
            }

            //跳转到指定页
            displayInfo.append("<input type=\"hidden\" id=\"pagesearchcurrentpage\" value=\"" + index + "\">");
            displayInfo.append("<input type=\"hidden\" id=\"pagesearchmax\" value=\"" + totalPage + "\">");
            displayInfo.append("<span class=\"page_word\">跳到</span>");

            displayInfo.append("<input id=\"pagesearchvalue\" value=\"" + index + "\" ");
            displayInfo.append("onkeyup=\"this.value=this.value.replace(/\\D/g,'')\" ");
            displayInfo.append("onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" ");
            displayInfo.append("class=\"txt w-30\" type=\"text\">");

            displayInfo.append("<span class=\"page_word\">页</span>");
            displayInfo.append("<a href=\"javascript:void(0);\" class=\"page_next\" id=\"pagesearch\">确定</a>");
            displayInfo.append("</div></div>");
        }
        return displayInfo.toString();
    }

    public static <E> String GetHtml(IPage<E> page, String path) {
        PageHelper pageHelper = new PageHelper();
        pageHelper.setTotal((int) page.getTotal());
        long index = page.getCurrent() <= 1 ? 1 : page.getCurrent();
        pageHelper.setIndex(index);
        long pagesize = page.getSize() <= 0 ? 20 : page.getSize();
        pageHelper.setPageSize(pagesize);
        pageHelper.setPath(path);
        return pageHelper.getManagerPage();
    }

    public static <E> String GetAjaxHtml(IPage<E> page, String path) {
        PageHelper pageHelper = new PageHelper();
        pageHelper.setTotal(page.getTotal());
        pageHelper.setIndex(page.getCurrent());
        pageHelper.setPageSize(page.getSize());
        pageHelper.setPath(path);
        //return pageHelper.getAjaxManagerPage();
        //如果一个页面中有多个分页，为了支持跳转，需要区分id
        String result = pageHelper.getAjaxDYXPManagerPage();
        String serfix = path.replace("javascript:", "").replace("(##);", "").replace("(#@);", "");
        result = result.replace("pagesearchcurrentpage", "pagesearchcurrentpage_" + serfix);
        result = result.replace("pagesearchmax", "pagesearchmax_" + serfix);
        result = result.replace("pagesearchvalue", "pagesearchvalue_" + serfix);
        result = result.replace("pagesearch\"", "pagesearch_" + serfix + "\"");
        return result;
    }
//
//    public static <E> String GetAjaxHtml(long pageIndex, long pageSize, long total, String path) {
//        PageHelper pageHelper = new PageHelper();
//        pageHelper.setTotal((int) total);
//        pageHelper.setIndex((int) pageIndex);
//        pageHelper.setPageSize((int) pageSize);
//        pageHelper.setPath(path);
//        //return pageHelper.getAjaxManagerPage();
//        //如果一个页面中有多个分页，为了支持跳转，需要区分id
//        String result = pageHelper.getAjaxDYXPManagerPage();
//        String serfix = path.replace("javascript:", "").replace("(##);", "").replace("(#@);", "");
//        result = result.replace("pagesearchcurrentpage", "pagesearchcurrentpage_" + serfix);
//        result = result.replace("pagesearchmax", "pagesearchmax_" + serfix);
//        result = result.replace("pagesearchvalue", "pagesearchvalue_" + serfix);
//        result = result.replace("pagesearch\"", "pagesearch_" + serfix + "\"");
//        return result;
//    }

    /**
     * 计算起始页和结束页
     */
    private void countPages() {

        if (index - displayNum / 2 < 1) {
            startPage = 1;
            endPage = displayNum > totalPage ? totalPage : displayNum;
        } else if (index + displayNum / 2 > totalPage) {
            long n = totalPage - displayNum + 1;
            startPage = n > 0 ? n : 1;
            endPage = totalPage;
        } else {
            startPage = index - displayNum / 2;
            endPage = startPage + displayNum - 1;
        }
    }

    /**
     * @param pageHTML the pageHTML to set
     */
    private void setPageHTML(String pageHTML) {
        this.pageHTML = pageHTML;
    }

    private String getAjaxDYXPManagerPageBusiness() {
        totalPage = getTotalPage();
        StringBuffer displayInfo = new StringBuffer();
        if (totalPage != 0 && pageSize != 0) {
            displayInfo
                    .append("<div class=\"layui-box layui-laypage layui-laypage-default\">");
            displayInfo.append("<span class=\"layui-laypage-count\">共" + totalPage + "页</span>");
            displayInfo.append("<span class=\"layui-laypage-skip\"> 跳到");
            displayInfo.append("<input id=\"pagesearchvalue\" value=\"" + index + "\" ");
            displayInfo.append("onkeyup=\"this.value=this.value.replace(/\\D/g,'')\" ");
            displayInfo.append("onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\" ");
            displayInfo.append("class=\"layui-input\" type=\"text\">页");
            displayInfo.append("<input type=\"button\" id=\"pagesearch\" class=\"layui-laypage-btn\" value=\"确定\"></span>");

            //跳转到指定页
            displayInfo.append("<input type=\"hidden\" id=\"pagesearchcurrentpage\" value=\"" + index + "\">");
            displayInfo.append("<input type=\"hidden\" id=\"pagesearchmax\" value=\"" + totalPage + "\">");

            // 判断如果当前是第一页 则“首页”和“第一页”失去链接
            if (index <= 1) {
                displayInfo.append("<a href=\"javascript:void(0);\" class=\"layui-laypage-prev disabled\" aria-controls=\"editable\" tabindex=\"0\">首页</a>");
                displayInfo.append("<a href=\"javascript:void(0);\" class=\"layui-laypage-prev disabled\" aria-controls=\"editable\" tabindex=\"0\">上一页</a>");
            } else {
                displayInfo.append("<a href=\"" + getPageNum(1) + "\" class=\"layui-laypage-prev disabled\" aria-controls=\"editable\" tabindex=\"0\">首页</a>");
                displayInfo.append("<a href=\"" + getPageNum(index - 1) + "\" class=\"layui-laypage-prev disabled\" aria-controls=\"editable\" tabindex=\"0\">上一页</a>");
            }
            countPages();
            for (long i = startPage; i <= endPage; i++) {
                if (i == index) {

                    displayInfo.append("<span class=\"layui-laypage-curr\"><em class=\"layui-laypage-em\"></em><em>" + i + "</em></span>");
                    /*displayInfo.append("<a href=\"javascript:void(0);\" class=\"paginate_button active\" aria-controls=\"editable\" tabindex=\"0\">"
                                    + i + "</a>");*/
                } else {
                    displayInfo.append("<a href=\"" + getPageNum(i) + "\" class=\"paginate_button\" aria-controls=\"editable\" tabindex=\"0\">" + i + "</a>");
                }
            }
            if (index >= totalPage) {
                displayInfo.append("<a href=\"javascript:void(0);\" class=\"paginate_button next\" aria-controls=\"editable\" tabindex=\"0\">下一页</a>");
                displayInfo.append("<a href=\"javascript:void(0);\" class=\"paginate_button next\" aria-controls=\"editable\" tabindex=\"0\">末页</a>");
            } else {
                displayInfo.append("<a href=\"" + getPageNum(index + 1) + "\" class=\"paginate_button next\" aria-controls=\"editable\" tabindex=\"0\">下一页</a>");
                displayInfo.append("<a href=\"" + getPageNum(totalPage) + "\" class=\"paginate_button next\" aria-controls=\"editable\" tabindex=\"0\">末页</a>");
            }

            //下拉选择
            displayInfo.append("<span class=\"layui-laypage-limits\">\n" +
                    "            <select lay-ignore=\"\" id=\"pagesearchpagesize\">\n" +
                    "              <option value='20' " + (pageSize == 20 ? "selected=\"selected\"" : "") + ">20 条/页</option>\n" +
                    "              <option value='50' " + (pageSize == 50 ? "selected=\"selected\"" : "") + ">50 条/页</option>\n" +
                    "              <option value='100' " + (pageSize == 100 ? "selected=\"selected\"" : "") + ">100 条/页</option>\n" +
                    "              <option value='200' " + (pageSize == 200 ? "selected=\"selected\"" : "") + ">200 条/页</option>\n" +
                    "            </select>\n" +
                    "           </span>");

            displayInfo.append("<span class=\"layui-laypage-count\">总共" + total + "条记录</span>");
            displayInfo.append("<div class=\"clear\"></div></div>");
        }
        return displayInfo.toString();
    }
}
