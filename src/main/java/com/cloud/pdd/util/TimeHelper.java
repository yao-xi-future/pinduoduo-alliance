package com.cloud.pdd.util;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

/// <summary>
/// 时间相关
/// Add by 成长的小猪（Jason.Song） on 2018/05/10
/// http://blog.csdn.net/jasonsong2008
/// </summary>
@Service
public class TimeHelper {
    /// <summary>
    /// 获取当前时间戳
    /// Add by 成长的小猪（Jason.Song） on 2019/05/07
    /// http://blog.csdn.net/jasonsong2008
    /// </summary>
    /// <param name="millisecond">精度（毫秒）设置 true，则生成13位的时间戳；精度（秒）设置为 false，则生成10位的时间戳；默认为 true </param>
    /// <returns></returns>
    public static long toTimeStamp(String time) {
        DateTime parse = DateUtil.parse(time, "yyyy-MM-dd HH:mm:ss");
        return parse.getTime();
    }

    //
    /// <summary>
    /// 转换指定时间得到对应的时间戳
    /// Add by 成长的小猪（Jason.Song） on 2019/05/07
    /// http://blog.csdn.net/jasonsong2008
    /// </summary>
    /// <param name="dateTime"></param>
    /// <param name="millisecond">精度（毫秒）设置 true，则生成13位的时间戳；精度（秒）设置为 false，则生成10位的时间戳；默认为 true </param>
    /// <returns>返回对应的时间戳</returns>
    public static Date toDateTime(Long timeStamp) {
        if (timeStamp == null) {
            return null;
        }
        if (StrUtil.length(timeStamp + "") < 13) {
            //不足13位需要补位；
            timeStamp = timeStamp * 1000;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//这个是你要转成后的时间的格式
        String sd = sdf.format(new Date(timeStamp));   // 时间戳转换成时间
        return DateUtil.parse(sd);
    }
}