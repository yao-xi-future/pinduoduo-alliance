package com.cloud.pdd.util;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;

import javax.servlet.http.HttpServletRequest;

public class BlogPageHelper {
    @Data
    private class PageModel {
        private int startPage;
        private int endPage;
        private int displayNum = 3;
    }

    public String getBlogPageHTML(Page page, HttpServletRequest request) {
        int total = Convert.toInt(page.getTotal());
        if (total <= 0) {
            return "没有找到相关的内容";
        }
        int pageSize = Convert.toInt(page.getSize());
        int totalPage = (total + pageSize - 1) / pageSize;
        int current = Convert.toInt(page.getCurrent());
        StringBuffer displayInfo = new StringBuffer();
        displayInfo.append("<a title=\"Total record\"><b>共" + total + "条</b></a>");
        // 判断如果当前是第一页 则“首页”和“第一页”失去链接
        if (current > 1) {
            displayInfo.append("<a href=\"" + convertListUrl(request, 1) + "\">首页</a>");
            displayInfo.append("<a href=\"" + convertListUrl(request, current - 1) + "\">上一页</a>");
        }
        PageModel pageModel = new PageModel();
        countPages(current, totalPage, pageModel);
        for (int i = pageModel.getStartPage(); i <= pageModel.getEndPage(); i++) {
            if (i == current) {
                displayInfo.append("<b>" + i + "</b>");
            } else {
                displayInfo.append("<a href=\"" + convertListUrl(request, i) + "\">" + i + "</a>");
            }
        }
        if (current < totalPage) {
            displayInfo.append("<a href=\"" + convertListUrl(request, current + 1) + "\">下一页</a>");
            displayInfo.append("<a href=\"" + convertListUrl(request, totalPage) + "\">尾页</a>");
        }
        return displayInfo.toString();
    }

    public String convertListUrl(HttpServletRequest request, int current) {
        String uri = request.getRequestURI();
        String queryString = request.getQueryString();
        if (StrUtil.isEmpty(queryString)) {
            return uri + ".html?current=" + current;
        }
        if (queryString.contains("keyword")) {
            String keyword = ReUtil.getGroup1("keyword=(\\w+)&", queryString);
            if (StrUtil.isEmpty(keyword)) {
                keyword = ReUtil.getGroup1("keyword=(\\w+)", queryString);
            }
            return uri + ".html?keyword=" + keyword + "&current=" + current;
        }
        if (queryString.contains("cOneId")) {
            String cOneId = ReUtil.getGroup1("cOneId=(\\w+)&", queryString);
//            String current = ReUtil.getGroup1("current=(\\w+)", queryString);
            return uri + "/1/" + cOneId + "/" + current + ".html";
        }
        if (queryString.contains("cTwoId")) {
            String cTwoId = ReUtil.getGroup1("cTwoId=(\\w+)&", queryString);
//            String current = ReUtil.getGroup1("current=(\\w+)", queryString);
            return uri + "/1/" + cTwoId + "/" + current + ".html";
        }
        return "";
    }

    /**
     * 计算起始页和结束页
     */
    private void countPages(int current, int totalPage, PageModel pageModel) {
        if (current - pageModel.getDisplayNum() / 2 < 1) {
            pageModel.setStartPage(1);
            pageModel.setEndPage(pageModel.getDisplayNum() > totalPage ? totalPage : pageModel.getDisplayNum());
        } else if (current + pageModel.getDisplayNum() / 2 > totalPage) {
            int n = totalPage - pageModel.getDisplayNum() + 1;
            pageModel.setStartPage(n > 0 ? n : 1);
            pageModel.setEndPage(totalPage);
        } else {
            pageModel.setStartPage(current - pageModel.getDisplayNum() / 2);
            pageModel.setEndPage(pageModel.getStartPage() + pageModel.getDisplayNum() - 1);
        }
    }
}
