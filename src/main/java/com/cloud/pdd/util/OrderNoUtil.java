package com.cloud.pdd.util;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class OrderNoUtil {
    /**
     * 获取文章购买单号
     */
    public String getArticle() {
        String format = DateUtil.format(new Date(), "yyyyMMddHHmmss");
        String numbers = RandomUtil.randomNumbers(5);
        String orderId = "A-" + format + numbers;
        return orderId;
    }
    //提现单号
    public String getWithdrawalOrderNo() {
        String format = DateUtil.format(new Date(), "yyyyMMddHHmmss");
        String numbers = RandomUtil.randomNumbers(5);
        String orderId = "WD-" + format + numbers;
        return orderId;
    }
}
