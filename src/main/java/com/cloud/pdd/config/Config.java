package com.cloud.pdd.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

/**
 * @Name Config
 * @Description
 * @Author douhaichao
 * @Date 2019/3/14 14:45
 * @Version 1.0
 **/
@AllArgsConstructor
@Data
@NoArgsConstructor
@Component
public class Config {
    @Value("${webSite}")
    private String webSite;

    @Value("${staticSite}")
    private String staticSite;

    @Value("${staticVersion:1}")
    private String staticVersion;

    @Value("${defaultPageSize:20}")
    private String defaultPageSize;

    @Value("${imgUrl}")
    private String imgUrl;

    @Value("${isDebug:0}")
    private Integer isDebug;

    @Value("${tempImgPath:}")
    private String tempImgPath;

    @Value("${wwwSite:}")
    private String wwwSite;

    @Value("${pdd.clientId:}")
    private String pddClientId;

    @Value("${pdd.clientSecret:}")
    private String pddClientSecret;

    @Value("${pdd.pid:}")
    private String pddPid;

    @Value("${pdd.uid:}")
    private String pddUid;

//    用户收入比例，用户将获取100%
    @Value("${pdd.incomeRate:100}")
    private BigDecimal incomeRate;

    // 最小提现金额
    @Value("${minWithdraw:10}")
    private BigDecimal minWithdraw;

    // 最小提现金额
    @Value("${redisVersion:}")
    private BigDecimal redisVersion;

}
