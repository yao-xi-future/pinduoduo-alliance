package com.cloud.pdd.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 *
 *
 * @Package: com.*.*.config
 * @ClassName: LoginConfig
 * @Description:拦截器配置
 * @author: zk
 * @date: 2019年9月19日 下午2:18:35
 */
@Configuration
public class LoginConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册TestInterceptor拦截器
        InterceptorRegistration registration = registry.addInterceptor(new AdminInterceptor());
        registration.addPathPatterns("/center");                      //所有路径都被拦截
        registration.addPathPatterns("/center/**");                      //所有路径都被拦截
        registration.excludePathPatterns(                         //添加不拦截路径
//                "/ajax/user/login",            //登录
//                "/ajax/user/getCode",            //获取验证码
//                "/login",            //登录
//                "/reg",            //登录
                "/**/*.html",            //html静态资源
                "/**/*.png",            //html静态资源
                "/**/*.js",              //js静态资源
                "/**/*.css",             //css静态资源
                "/**/*.woff",
                "/**/*.ttf",
                "/**/*.svg",
                "/**/*.ico",
                "/favicon.ico"
                );
    }

}