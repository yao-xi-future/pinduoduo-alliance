package com.cloud.pdd.service;

import com.cloud.pdd.model.domain.PddOrderDetailRequest;
import com.cloud.pdd.model.entity.PddOrder;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cloud.pdd.model.request.PddCenterOrderListRequest;
import com.cloud.pdd.model.request.PddOrderListRequest;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkOrderDetailGetResponse;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkOrderListIncrementGetResponse;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.util.List;

/**
 * 拼多多订单
 *
 * @author douhaichao code generator
 * @date 2021-04-14 11:10:20
 */
public interface PddOrderService extends IService<PddOrder> {
    /**
     * 最后更新时间段增量同步推广订单信息
     */
    PddDdkOrderListIncrementGetResponse getOrderListIncrement(
            PddOrderListRequest req);

    /**
     * 查询订单详情
     */
    PddDdkOrderDetailGetResponse getOrderDetail(HttpServletRequest httpServletRequest,
                                                PddOrderDetailRequest request);

    void syncOrder(Integer day);

    void syncOrderByDay(String dateTime);

    IPage<PddOrder> selectList(Page p, PddCenterOrderListRequest req);

    void pay();

    String getCenterOrderNav(String orderStatus);

}
