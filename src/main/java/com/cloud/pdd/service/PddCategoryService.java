package com.cloud.pdd.service;

import com.pdd.pop.sdk.http.api.pop.response.PddGoodsCatsGetResponse;

import java.util.List;

/**
 * 商品分类
 */
public interface PddCategoryService {

    /**
     * 商品标准类目接口
     */
    List<PddGoodsCatsGetResponse.GoodsCatsGetResponseGoodsCatsListItem> getAllCategory();

    String getHomeCategoryHtml();

    String getCategoryListHtml(Integer currentId);

}
