package com.cloud.pdd.service;

import com.cloud.pdd.model.domain.ProductUrlGenRequest;
import com.cloud.pdd.model.request.PddProductInfoRequest;
import com.cloud.pdd.model.request.PddProductSearchRequest;
import com.cloud.pdd.model.request.PddProductRecommendListRequest;
import com.pdd.pop.sdk.http.api.pop.request.PddDdkGoodsPromotionUrlGenerateRequest;
import com.pdd.pop.sdk.http.api.pop.response.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;

public interface PddProductService {
    /**
     * 获取列表页数据
     */
    PddDdkGoodsSearchResponse.GoodsSearchResponse getProductSearch(PddProductSearchRequest req);

    /**
     * 获取详情页数据
     */
    PddDdkGoodsDetailResponse.GoodsDetailResponse getProductDetail(
            PddProductInfoRequest req);

    /**
     * 获取转链地址,通过goodsSign生成链接地址
     */
    PddDdkGoodsPromotionUrlGenerateResponse.GoodsPromotionUrlGenerateResponse getProductPromotionUrlGenerate(
            PddDdkGoodsPromotionUrlGenerateRequest req);

    /**
     * 获取转链地址,通过拼多多url生成链接地址
     */
    PddDdkGoodsZsUnitUrlGenResponse.GoodsZsUnitGenerateResponse getProductUnitUrlGenerate(
            HttpServletRequest httpServletRequest, ProductUrlGenRequest req);

    /**
     * 查询商品标签列表,查询首页所有分类API
     */
    PddGoodsOptGetResponse.GoodsOptGetResponse getProductOpt();

    /**
     * 多多进宝商品推荐API
     * */
    PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponse getProductRecommend(PddProductRecommendListRequest req);

    /**
     * 获取首页官方爆品
    */
//    PddDdkGoodsRecommendGetResponse.GoodsBasicDetailResponse getProductGfbp(PddProductRecommendListRequest pddProductRecommendListRequest);

    void setProductPidGenerate(Long mediaId);

//    通过实际佣金计算预计佣金
//    BigDecimal getProductCommission(Long promotionAmount);

    String getProductListData(PddProductSearchRequest req,
                              List<PddDdkGoodsSearchResponse.GoodsSearchResponseGoodsListItem> goodsList);
}
