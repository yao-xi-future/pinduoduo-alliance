package com.cloud.pdd.service;

import com.cloud.pdd.model.entity.PddWithdrawalOrders;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cloud.pdd.model.request.PddCenterWithdrawListRequest;

import java.math.BigInteger;
import java.util.List;

/**
 * 提现订单
 *
 * @author douhaichao code generator
 * @date 2021-04-14 11:48:31
 */
public interface PddWithdrawalOrdersService extends IService<PddWithdrawalOrders> {

    IPage<PddWithdrawalOrders> selectList(Page p, PddCenterWithdrawListRequest req);

    int applyWithdraw(PddWithdrawalOrders req);
}
