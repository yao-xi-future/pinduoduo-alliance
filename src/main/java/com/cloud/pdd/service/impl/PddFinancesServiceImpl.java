package com.cloud.pdd.service.impl;

import com.cloud.pdd.model.entity.PddFinances;
import com.cloud.pdd.mapper.PddFinancesMapper;
import com.cloud.pdd.service.PddFinancesService;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import cn.hutool.core.bean.BeanUtil;
import java.math.BigInteger;
import java.util.List;

/**
 * 拼多多财务
 *
 * @author douhaichao code generator
 * @date 2021-04-14 13:47:29
 */
@Service
@AllArgsConstructor
public class PddFinancesServiceImpl extends ServiceImpl<PddFinancesMapper, PddFinances> implements PddFinancesService {

    @Override
    public IPage<PddFinances> selectList(Page page, Object req) {
        PddFinances model = new PddFinances();
        BeanUtil.copyProperties(req, model);
        QueryWrapper<PddFinances> queryWrapper = new QueryWrapper<PddFinances>();
        queryWrapper.orderByDesc("user_id");
        IPage<PddFinances> list = baseMapper.selectPage(
                page, queryWrapper
        );
        return list;
    }
}
