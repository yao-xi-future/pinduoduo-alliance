package com.cloud.pdd.service.impl;

import com.cloud.pdd.model.entity.PddUserRecords;
import com.cloud.pdd.mapper.PddUserRecordsMapper;
import com.cloud.pdd.model.request.PddCenterRecordstListRequest;
import com.cloud.pdd.service.PddUserRecordsService;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import cn.hutool.core.bean.BeanUtil;
import java.math.BigInteger;
import java.util.List;

/**
 * 用户订单流水
 *
 * @author douhaichao code generator
 * @date 2021-04-14 11:48:33
 */
@Service
@AllArgsConstructor
public class PddUserRecordsServiceImpl extends ServiceImpl<PddUserRecordsMapper, PddUserRecords> implements PddUserRecordsService {

    @Override
    public IPage<PddUserRecords> selectList(Page page, PddCenterRecordstListRequest req) {
        QueryWrapper<PddUserRecords> queryWrapper = new QueryWrapper<PddUserRecords>();
        queryWrapper.eq("user_id",req.getUserId());
        queryWrapper.orderByDesc("id");
        IPage<PddUserRecords> list = baseMapper.selectPage(
                page, queryWrapper
        );
        return list;
    }
}
