package com.cloud.pdd.service.impl;

import com.cloud.pdd.config.Config;
import com.pdd.pop.sdk.http.PopClient;
import com.pdd.pop.sdk.http.PopHttpClient;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class PddCommonServiceImpl {
    private static PopClient popClient = null;
    @Autowired
    Config config;

    public PopClient getClient() {
//        if (popClient == null) {
            String clientId = config.getPddClientId();
            String clientSecret = config.getPddClientSecret();
            PopClient client = new PopHttpClient(clientId, clientSecret);
            popClient = client;
//        }
        return popClient;
    }

}
