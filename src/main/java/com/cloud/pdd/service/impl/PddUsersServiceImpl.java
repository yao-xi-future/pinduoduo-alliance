package com.cloud.pdd.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.json.JSONUtil;
import com.cloud.common.security.AES;
import com.cloud.common.util.CookieUtils;
import com.cloud.pdd.config.Config;
import com.cloud.pdd.mapper.PddUsersMapper;
import com.cloud.pdd.model.domain.UserInfo;
import com.cloud.pdd.model.entity.PddUsers;
import com.cloud.pdd.model.enums.CookieConst;
import com.cloud.pdd.service.PddUsersService;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cloud.pdd.util.PddUtil;
import com.pdd.pop.sdk.common.util.JsonUtil;
import com.pdd.pop.sdk.http.PopClient;
import com.pdd.pop.sdk.http.api.pop.request.PddDdkGoodsPidGenerateRequest;
import com.pdd.pop.sdk.http.api.pop.request.PddDdkMemberAuthorityQueryRequest;
import com.pdd.pop.sdk.http.api.pop.request.PddDdkPidMediaidBindRequest;
import com.pdd.pop.sdk.http.api.pop.request.PddDdkRpPromUrlGenerateRequest;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkGoodsPidGenerateResponse;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkMemberAuthorityQueryResponse;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkPidMediaidBindResponse;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkRpPromUrlGenerateResponse;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * 博客用户
 *
 * @author douhaichao code generator
 * @date 2021-02-25 17:54:12
 */
@Service
@AllArgsConstructor
public class PddUsersServiceImpl extends ServiceImpl<PddUsersMapper, PddUsers> implements PddUsersService {
    private final PddCommonServiceImpl pddCommonService;
    private final Config config;
    private final PddUtil pddUtil;

    @Override
    public UserInfo getUserInfo(HttpServletRequest request) {
        String cookie = CookieUtils.getCookie(request, CookieConst.loginPddCookie);
        if (StrUtil.isEmpty(cookie)) {
            return null;
        }
        String s = AES.aesDecrypt(cookie);
        return JSONUtil.toBean(s, UserInfo.class);
    }

    @Override
    public Integer getUserId(HttpServletRequest request) {
        UserInfo userInfo = getUserInfo(request);
        if (userInfo == null) {
            return -1;
        }
        return userInfo.getId();
    }

    //    https://open.pinduoduo.com/application/document/api?id=pdd.ddk.pid.mediaid.bind&permissionId=2
    @SneakyThrows
    @Override
    public PddDdkGoodsPidGenerateResponse setUserMediaidBind(
            Integer userId) {
        PopClient client = pddCommonService.getClient();
//        PddDdkPidMediaidBindRequest request = new PddDdkPidMediaidBindRequest();
//        request.setMediaId(Convert.toLong(userId));
//        List<String> pidList = new ArrayList<String>();
//        pidList.add(Convert.toStr(userId));
//        request.setPidList(pidList);
//        PddDdkPidMediaidBindResponse response = client.syncInvoke(request);

        PddDdkGoodsPidGenerateRequest request = new PddDdkGoodsPidGenerateRequest();
        request.setNumber(Convert.toLong(userId));
        List<String> pIdNameList = new ArrayList<String>();
        pIdNameList.add(Convert.toStr(userId));
        request.setPIdNameList(pIdNameList);
        request.setMediaId(Convert.toLong(userId));
        PddDdkGoodsPidGenerateResponse response = client.syncInvoke(request);
//        System.out.println(JsonUtil.transferToJson(response));
        return response;
    }

    @SneakyThrows
    @Override
    public String getBeiAnUrl(Integer userId) {
        String userIdStr = pddUtil.getCustomParameters();
        PopClient client = pddCommonService.getClient();
        PddDdkMemberAuthorityQueryRequest request = new PddDdkMemberAuthorityQueryRequest();
        request.setPid(config.getPddPid());
        request.setCustomParameters(userIdStr);
        PddDdkMemberAuthorityQueryResponse response = client.syncInvoke(request);
        Integer bind = response.getAuthorityQueryResponse().getBind();
        if (bind == 1) {
            return "";
        }
        //如果是没备案
        PddDdkRpPromUrlGenerateRequest requestBeiAn = new PddDdkRpPromUrlGenerateRequest();
        requestBeiAn.setChannelType(10);
        List<String> pIdList = new ArrayList<>();
        pIdList.add(config.getPddPid());
        requestBeiAn.setCustomParameters(userIdStr);
        requestBeiAn.setPIdList(pIdList);
        requestBeiAn.setGenerateWeApp(true);
        PddDdkRpPromUrlGenerateResponse responseBeiAn = client.syncInvoke(requestBeiAn);
        List<PddDdkRpPromUrlGenerateResponse.RpPromotionUrlGenerateResponseUrlListItem> urlList = responseBeiAn.getRpPromotionUrlGenerateResponse().getUrlList();
        if (urlList == null) {
            return "";
        }
        String url = urlList.get(0).getUrl();
        return url;
    }
}
