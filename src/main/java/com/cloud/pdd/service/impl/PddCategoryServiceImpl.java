package com.cloud.pdd.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.json.JSONUtil;
import com.cloud.pdd.model.enums.ProductCategoryEnum;
import com.cloud.pdd.service.PddCategoryService;
import com.cloud.pdd.service.PddOrderService;
import com.pdd.pop.sdk.common.util.JsonUtil;
import com.pdd.pop.sdk.http.PopClient;
import com.pdd.pop.sdk.http.api.pop.request.PddGoodsCatsGetRequest;
import com.pdd.pop.sdk.http.api.pop.response.PddGoodsCatsGetResponse;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class PddCategoryServiceImpl implements PddCategoryService {

    PddCommonServiceImpl commonService;

    //    https://open.pinduoduo.com/application/document/api?id=pdd.goods.cats.get&permissionId=2
    @SneakyThrows
    @Override
    public List<PddGoodsCatsGetResponse.GoodsCatsGetResponseGoodsCatsListItem> getAllCategory() {
        PopClient client = commonService.getClient();
        PddGoodsCatsGetRequest request = new PddGoodsCatsGetRequest();
        request.setParentCatId(0L);
        PddGoodsCatsGetResponse response = client.syncInvoke(request);
        String s = JSONUtil.toJsonStr(response);
        System.out.print(s);
        List<PddGoodsCatsGetResponse.GoodsCatsGetResponseGoodsCatsListItem> goodsCatsList = response.getGoodsCatsGetResponse().getGoodsCatsList();
        return goodsCatsList;
    }

    @Override
    public String getHomeCategoryHtml() {
        List<Map<String, String>> allCategory = ProductCategoryEnum.getAllEnum();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < allCategory.size(); i++) {
            Map<String, String> category = allCategory.get(i);
            if (i == 0 || i % 8 == 0) {
                sb.append("<ul class=\"swiper-slide\">");
            }
            String code = category.get("code");
            String desc = category.get("desc");
            String imgUrl = category.get("imgUrl");
            sb.append(" <li>");
            sb.append("<a href=\"/product/list?optId=" + code + "\" >");
            sb.append("<img src = \""+category.get("imgUrl")+"\" >");
            sb.append("<span>" + desc + "</span>");
            sb.append("</a></li>");
            if (i != 0 && i % 7 == 0) {
                sb.append("</ul>");
            }
        }
        return sb.toString();
    }

    @Override
    public String getCategoryListHtml(Integer currentId) {
        List<Map<String, String>> allCategory = ProductCategoryEnum.getAllEnum();
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < allCategory.size(); i++) {
            Map<String, String> category = allCategory.get(i);
            Integer code = Convert.toInt(category.get("code"));
            String desc = category.get("desc");
            String className = "";
            if (code.equals(currentId)) {
                className = "class=\"off\"";
            }
            sb.append("<li " + className + "><a href=\"?optId=" + code + "\">" + desc + "</a></li>");
        }
        return sb.toString();
    }
}
