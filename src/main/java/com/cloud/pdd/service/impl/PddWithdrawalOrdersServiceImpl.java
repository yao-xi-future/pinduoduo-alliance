package com.cloud.pdd.service.impl;

import cn.hutool.core.util.NumberUtil;
import com.cloud.pdd.config.Config;
import com.cloud.pdd.mapper.PddFinancesMapper;
import com.cloud.pdd.mapper.PddUserRecordsMapper;
import com.cloud.pdd.model.entity.PddFinances;
import com.cloud.pdd.model.entity.PddUserRecords;
import com.cloud.pdd.model.entity.PddWithdrawalOrders;
import com.cloud.pdd.mapper.PddWithdrawalOrdersMapper;
import com.cloud.pdd.model.request.PddCenterWithdrawListRequest;
import com.cloud.pdd.service.PddFinancesService;
import com.cloud.pdd.service.PddUserRecordsService;
import com.cloud.pdd.service.PddWithdrawalOrdersService;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import cn.hutool.core.bean.BeanUtil;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

/**
 * 提现订单
 *
 * @author douhaichao code generator
 * @date 2021-04-14 11:48:31
 */
@Service
@AllArgsConstructor
public class PddWithdrawalOrdersServiceImpl extends ServiceImpl<PddWithdrawalOrdersMapper, PddWithdrawalOrders> implements PddWithdrawalOrdersService {

    private final PddFinancesMapper pddFinancesMapper;
    private final Config config;
    private final PddUserRecordsMapper pddUserRecordsMapper;

    @Override
    public IPage<PddWithdrawalOrders> selectList(Page page, PddCenterWithdrawListRequest req) {
        QueryWrapper<PddWithdrawalOrders> queryWrapper = new QueryWrapper<PddWithdrawalOrders>();
        queryWrapper.eq("user_id", req.getUserId());
        queryWrapper.orderByDesc("id");
        IPage<PddWithdrawalOrders> list = baseMapper.selectPage(
                page, queryWrapper
        );
        return list;
    }

    //申请提现
    @Transactional
    @Override
    public int applyWithdraw(PddWithdrawalOrders req) {
        if (req.getCash().compareTo(config.getMinWithdraw()) < 0) {
            //提现最小金额为 X 元
            return -2;
        }
        int result = pddFinancesMapper.applyWithdraw(req);
        if (result <= 0) {
            //余额不足
            return -1;
        }
        Integer userId = req.getUserId();
        //查询余额
        QueryWrapper queryWrapperFinance = new QueryWrapper();
        queryWrapperFinance.eq("user_id", userId);
        PddFinances userFinances = pddFinancesMapper.selectOne(queryWrapperFinance);
        //增加流水
        PddUserRecords pddUserRecords = new PddUserRecords();
        pddUserRecords.setIncome(NumberUtil.mul(req.getCash(), -1));
        pddUserRecords.setRemark("申请提现：" + req.getCash());
        BigDecimal surplus = NumberUtil.sub(userFinances.getMoney(), req.getCash());
        pddUserRecords.setSurplus(surplus);
        pddUserRecords.setUserId(userId);
        pddUserRecordsMapper.insert(pddUserRecords);
        //增加提现记录
        baseMapper.insert(req);
        return 1;
    }
}
