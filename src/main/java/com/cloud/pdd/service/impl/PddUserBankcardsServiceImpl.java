package com.cloud.pdd.service.impl;

import com.cloud.pdd.model.entity.PddUserBankcards;
import com.cloud.pdd.mapper.PddUserBankcardsMapper;
import com.cloud.pdd.service.PddUserBankcardsService;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import cn.hutool.core.bean.BeanUtil;
import java.math.BigInteger;
import java.util.List;

/**
 * 用户卡号绑定
 *
 * @author douhaichao code generator
 * @date 2021-04-14 11:48:29
 */
@Service
@AllArgsConstructor
public class PddUserBankcardsServiceImpl extends ServiceImpl<PddUserBankcardsMapper, PddUserBankcards> implements PddUserBankcardsService {

    @Override
    public IPage<PddUserBankcards> selectList(Page page, Object req) {
        PddUserBankcards model = new PddUserBankcards();
        BeanUtil.copyProperties(req, model);
        QueryWrapper<PddUserBankcards> queryWrapper = new QueryWrapper<PddUserBankcards>();
        queryWrapper.orderByDesc("id");
        IPage<PddUserBankcards> list = baseMapper.selectPage(
                page, queryWrapper
        );
        return list;
    }
}
