package com.cloud.pdd.service.impl;

import com.cloud.pdd.model.entity.PddFavorite;
import com.cloud.pdd.mapper.PddFavoriteMapper;
import com.cloud.pdd.service.PddFavoriteService;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import cn.hutool.core.bean.BeanUtil;
import java.math.BigInteger;
import java.util.List;

/**
 * 我的收藏
 *
 * @author douhaichao code generator
 * @date 2021-04-16 01:10:44
 */
@Service
@AllArgsConstructor
public class PddFavoriteServiceImpl extends ServiceImpl<PddFavoriteMapper, PddFavorite> implements PddFavoriteService {

    @Override
    public IPage<PddFavorite> selectList(Page page) {
        PddFavorite model = new PddFavorite();
        QueryWrapper<PddFavorite> queryWrapper = new QueryWrapper<PddFavorite>();
        queryWrapper.orderByDesc("id");
        IPage<PddFavorite> list = baseMapper.selectPage(
                page, queryWrapper
        );
        return list;
    }
}
