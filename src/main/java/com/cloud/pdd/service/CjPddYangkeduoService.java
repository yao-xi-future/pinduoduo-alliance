package com.cloud.pdd.service;

import com.cloud.sh.model.entity.CjPddYangkeduo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cloud.pdd.model.request.CjPddListRequest;

import java.math.BigInteger;
import java.util.List;

/**
 * 采集拼多多商品
 *
 * @author douhaichao code generator
 * @date 2021-03-18 20:59:14
 */
public interface CjPddYangkeduoService extends IService<CjPddYangkeduo> {

    int count(CjPddYangkeduo req);

    List<CjPddYangkeduo> selectAll(CjPddYangkeduo req);

    IPage<CjPddYangkeduo> selectPage(Page page, CjPddYangkeduo req);

    IPage<CjPddYangkeduo> selectPageByLike(Page page, CjPddYangkeduo req);

    CjPddYangkeduo selectOne(CjPddYangkeduo req);

    IPage<CjPddYangkeduo> selectCjPddList(Page p, CjPddListRequest cjPddListRequest);

    void cjPddData(CjPddYangkeduo req);

}
