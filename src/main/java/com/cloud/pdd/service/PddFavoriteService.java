package com.cloud.pdd.service;

import com.cloud.pdd.model.entity.PddFavorite;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigInteger;
import java.util.List;

/**
 * 我的收藏
 *
 * @author douhaichao code generator
 * @date 2021-04-16 01:10:44
 */
public interface PddFavoriteService extends IService<PddFavorite> {

    IPage<PddFavorite> selectList(Page p);

}
