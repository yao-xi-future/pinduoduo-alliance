package com.cloud.pdd.service;

import com.cloud.pdd.model.domain.UserInfo;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cloud.pdd.model.entity.PddUsers;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkGoodsPidGenerateResponse;
import com.pdd.pop.sdk.http.api.pop.response.PddDdkPidMediaidBindResponse;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 拼多多用户
 *
 * @author douhaichao code generator
 * @date 2021-02-25 17:54:12
 */
public interface PddUsersService extends IService<PddUsers> {

    UserInfo getUserInfo(HttpServletRequest request);

    Integer getUserId(HttpServletRequest request);

    /**
     * 创建多多进宝推广位
     * */
    PddDdkGoodsPidGenerateResponse setUserMediaidBind(Integer userId);

    /**
     * 设置备案Url
     * TODO 这里需要注意，拿到PID的时候需要进行账号的备案，
     * 拿到这里的回调值以后要在浏览器里进行访问，
     * 备案成功以后才可以进行数据的推广，否则猜你喜欢等接口不可用。
     * */
    String getBeiAnUrl(Integer userId);
}
