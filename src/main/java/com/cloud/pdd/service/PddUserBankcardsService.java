package com.cloud.pdd.service;

import com.cloud.pdd.model.entity.PddUserBankcards;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigInteger;
import java.util.List;

/**
 * 用户卡号绑定
 *
 * @author douhaichao code generator
 * @date 2021-04-14 11:48:29
 */
public interface PddUserBankcardsService extends IService<PddUserBankcards> {

    IPage<PddUserBankcards> selectList(Page p, Object object);

}
