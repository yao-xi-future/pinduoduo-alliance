package com.cloud.pdd.service;

import com.cloud.pdd.model.entity.PddUserRecords;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cloud.pdd.model.request.PddCenterRecordstListRequest;

import java.math.BigInteger;
import java.util.List;

/**
 * 用户订单流水
 *
 * @author douhaichao code generator
 * @date 2021-04-14 11:48:33
 */
public interface PddUserRecordsService extends IService<PddUserRecords> {

    IPage<PddUserRecords> selectList(Page p, PddCenterRecordstListRequest req);

}
