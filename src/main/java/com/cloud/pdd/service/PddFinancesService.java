package com.cloud.pdd.service;

import com.cloud.pdd.model.entity.PddFinances;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigInteger;
import java.util.List;

/**
 * 拼多多财务
 *
 * @author douhaichao code generator
 * @date 2021-04-14 13:47:29
 */
public interface PddFinancesService extends IService<PddFinances> {

    IPage<PddFinances> selectList(Page p, Object object);

}
