//source:https://www.jq22.com/webqd2043
var page = 2, //分页码
    off_on = false, //分页开关(滚动加载方法 1 中用的)
    timers = null,
    listId = $("#listId").val(), //上次请求位置
    pageSize = 10, //每页显示的数量
    optId = $("#optId").val(), //类型
    sortType = $("#sortType").val(),
    showType = $("#showType").val()
; //定时器(滚动加载方法 2 中用的)

//加载数据
var LoadingDataFn = function () {
    $.post("/ajax/product/getData", {
        listId: listId,
        pageSize: pageSize,
        page: page,
        optId: optId,
        sortType: sortType,
        showType: showType
    }, function (d) {
        if (d.code != 0) {
            return;
        }
        listId = d.msg;
        $('#list_box').append(d.data);
    })
    off_on = true; //[重要]这是使用了 {滚动加载方法1}  时 用到的 ！！！[如果用  滚动加载方法1 时：off_on 在这里不设 true的话， 下次就没法加载了哦！]
};

//初始化， 第一次加载
$(document).ready(function () {
    LoadingDataFn();
});

//底部切换
$(document.body).on('click', '#footer div', function () {
    $(this).addClass('active').siblings().removeClass('active');
});

//滚动加载方法1
$('#main').scroll(function () {
    //当时滚动条离底部60px时开始加载下一页的内容
    if (($(this)[0].scrollTop + $(this).height() + 60) >= $(this)[0].scrollHeight) {
        //这里用 [ off_on ] 来控制是否加载 （这样就解决了 当上页的条件满足时，一下子加载多次的问题啦）
        if (off_on) {
            //off_on = false;
            //page++;
            //console.log("第"+page+"页");
            //LoadingDataFn();  //调用执行上面的加载方法
        }
    }
});

//滚动加载方法2
$('#main').scroll(function () {
    //当时滚动条离底部60px时开始加载下一页的内容
    if (($(this)[0].scrollTop + $(this).height() + 60) >= $(this)[0].scrollHeight) {
        clearTimeout(timers);
        //这里还可以用 [ 延时执行 ] 来控制是否加载 （这样就解决了 当上页的条件满足时，一下子加载多次的问题啦）
        timers = setTimeout(function () {
            page++;
            // console.log("第" + page + "页");
            LoadingDataFn(); //调用执行上面的加载方法
        }, 300);
    }
});

//还可以基window窗口（body）来添加滚动事件, (因为布局不同,所以在这里没效果，因为[上面是基于body中的某个元素来添加滚动事的])
$(window).scroll(function () {
    //当时滚动条离底部60px时开始加载下一页的内容
    if (($(window).height() + $(window).scrollTop() + 60) >= $(document).height()) {
        clearTimeout(timers);
        timers = setTimeout(function () {
            page++;
            // console.log("第" + page + "页");
            LoadingDataFn();
        }, 300);
    }
});