$(function () {
    $(".zy_login_box .zhuce_con>div").hide().first().show()
    var n = 0;
    var w = $(".zy_login_box .zhuce_style i").width()
    $(".zy_login_box .zhuce_style a").mousedown(
        function () {
            n = $(this).index()
            $(".zy_login_box .zhuce_con>div").hide().eq(n).fadeIn()
            $(".zy_login_box .zhuce_style i").animate({left: n * w}, 300)
            $(this).css("color", "#10805a")
            $(this).siblings().css("color", "#333")
        }
    );

    $(".validateImg").on('click', function () {
        $(".validateImg").attr("src", "/ajax/user/getCode?v=" + Math.random());
    });
    /**登录*/
    $(".zy_login_btn").click(function () {
        login();
    });
});


function login() {
    var data = {};
    data.mobile = $("#mobile").val();
    data.password = $("#password").val();
    data.code = $("#code").val();
    if (data.mobile.length != 11) {
        layer.msg('请正确输入手机号');
        return;
    }
    if (data.password == '') {
        layer.msg('密码不能为空');
        return false;
    }
    if (data.code == '') {
        $(".validateImg").attr("src", "/ajax/user/getCode?v=" + Math.random());
        layer.msg('验证码不能为空');
        return false;
    }
    $.post("/ajax/user/login", data, function (d) {
        if (d.msg == "success") {
            layer.msg('登录成功', function () {
                window.location = '/center';
                return true;
            });
        } else {
            layer.msg(d.msg, function () {
                $(".validateImg").attr("src", "/ajax/user/getCode?v=" + Math.random());
                return false;
            });
        }
    });
}