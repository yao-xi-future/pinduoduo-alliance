# 拼多多联盟

#### 介绍
拼多多联盟可获取实时的价格，佣金，抽佣，返利，订单完成后自动分佣。


#### 软件架构
本项目使用的是java1.8进行开发。
后端：SpringBoot2.1
前端：Thymeleaf
数据库：Mysql5.3
缓存：Redis3.2.1
负载均衡：Nginx


#### 安装教程
本项目在编译之前需要下载基础库：
https://gitee.com/yao-xi-future/xiats_common


#### 安装部署

具体的安装文档及部署文档：[http://www.xiats.com/list/2/21.html](http://www.xiats.com/list/2/21.html)

#### 测试地址
[http://pdd.xiats.com](http://pdd.xiats.com)


```
一、准备
技术栈说明：
本项目使用的是java1.8进行开发。
后端：SpringBoot2.1
前端：Thymeleaf
数据库：Mysql5.3
缓存：Redis3.2.1
负载均衡：Nginx
二、本地编译
1 maven配置
2 
二、安装与部署
1 jdk安装
2 nginx安装
	2.1 nginx配置
3 mysql的安装
4 Reids的安装 
5 阿里云oss申请
	5.1 申请 
	5.2 设置
6 拼多多开发者账号申请
7 拼多多备案申请
	7.1 为什么要备案？
	7.2 备案流程
8 配置的讲解
9 打包及发布
10 运行
11 完成
```


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

可进QQ群讨论：559431664
